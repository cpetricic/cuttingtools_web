<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>

<script>
$(document).ready(function() {
    if(sessionStorage.getItem("id_usuario")==null){
        top.location.href="home";
    }
    $('#nombre_datos').val(sessionStorage.getItem("nombre_usuario"));
    $('#apellido_datos').val(sessionStorage.getItem("apellido_usuario"));
    $('#telefono_datos').val(sessionStorage.getItem("telefono_usuario"));
    $('#email_datos').val(sessionStorage.getItem("email_usuario"));

    $('#region').change(function() {
        var regionId = $(this).val();
        if (regionId) {
            // $.getJSON('https://apis.digital.gob.cl/dpa/regiones/' + regionId + '/comunas', function(data) {
            //     var options = '';
            //     for (var i = 0; i < data.length; i++) {
            //     options += '<option value="' + data[i].codigo + '">' + data[i].nombre + '</option>';
            //     }
            //     $('#comuna').html('<option value="">Selecciona una comuna...</option>' + options);
            // });

            $.ajax({
                type: 'POST',
                crossDomain: true,
                dataType: 'jsonp',
                url: 'https://apis.digital.gob.cl/dpa/regiones/' + regionId + '/comunas',
                success: function(data){
                    var options = '';
                    for (var i = 0; i < data.length; i++) {
                        options += '<option value="' + data[i].codigo + '">' + data[i].nombre + '</option>';
                    }
                    $('#comuna').html('<option value="">Selecciona una comuna...</option>' + options);
                }
            });



        } else {
            $('#comuna').html('<option value="">Selecciona una comuna...</option>');
        }
    });
    get_regiones();
    get_direcciones();
    get_compras();
});
function get_regiones(){
    // $.getJSON('https://apis.digital.gob.cl/dpa/regiones', function(data) {
    //     var options = '';
    //     for (var i = 0; i < data.length; i++) {
    //         options += '<option value="' + data[i].codigo + '">' + data[i].nombre + '</option>';
    //     }
    //     $('#region').html('<option value="">Selecciona una región...</option>' + options);
    // });
    $.ajax({
        type: 'POST',
        crossDomain: true,
        dataType: 'jsonp',
        url: 'https://apis.digital.gob.cl/dpa/regiones',
        success: function(data){
            var options = '';
            for (var i = 0; i < data.length; i++) {
                options += '<option value="' + data[i].codigo + '">' + data[i].nombre + '</option>';
            }
            $('#region').html('<option value="">Selecciona una región...</option>' + options);
        }
    });
}

function pestana(num){
    $('.nav-link').removeClass('active');
    $('.nav-link').eq(num-1).addClass('active');
    $('.auth-card').hide();
    $('.auth-card').eq(num).fadeIn(200);
}
function actualizar(){
    var id_usuario = sessionStorage.getItem("id_usuario");
    var nombre = $('#nombre_datos').val();
    var apellido = $('#apellido_datos').val();
    var correo = $('#email_datos').val();
    var telefono = $('#telefono_datos').val();


    if (nombre == '') {
        error('Debe ingresar el nombre.');
        $('#nombre').focus();
        return;
    }
    if (apellido == '') {
        error('Debe ingresar el apellido.');
        $('#nombre').focus();
        return;
    }
    if (correo == '') {
        error('Debe ingresar el correo.');
        $('#correo').focus();
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'lib/modulo.php',
        data: {
            idfuncion: 3,
            nombre: nombre,
            apellido: apellido,
            correo: correo,
            telefono: telefono,
            id_usuario:id_usuario
        },
        dataType: 'json',
        success: function(resp) {
            //console.log(resp);
            switch (parseInt(resp.estado)) {
                case 0:
                    error('Se ha producido un error, vuelva a intentarlo más tarde.');
                break;
                case 1:
                    ok('Guardado correctamente.');
					sessionStorage.setItem("nombre_usuario", nombre);
					sessionStorage.setItem("apellido_usuario", apellido);
					sessionStorage.setItem("telefono_usuario", telefono);
					sessionStorage.setItem("email_usuario", correo);
                    set_nav_login();
                break;
                case 2:
                    error('El email ingresado ya se encuentra en uso');
                break;
            }
        }
    });
}
function cambiar_pass(){
    var id_usuario = sessionStorage.getItem("id_usuario");
    var pass1 = $('#pass1_datos').val();
    var pass2 = $('#pass2_datos').val();

    if (pass1 == '') {
        error('Debe ingresar la contraseña.');
        $('#pass1_datos').focus();
        return;
    }
    if (pass2 == '') {
        error('Debe confirmar la contraseña.');
        $('#pass2_datos').focus();
        return;
    }
    if (pass1 != pass2) {
        error('La contraseña no coincide.');
        $('#pass2_datos').focus();
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'lib/modulo.php',
        data: {
            idfuncion: 4,
            clave: pass1,
            id_usuario:id_usuario
        },
        dataType: 'json',
        success: function(resp) {
            //console.log(resp);
            switch (parseInt(resp.estado)) {
                case 0:
                    error('Se ha producido un error, vuelva a intentarlo más tarde.');
                break;
                case 1:
                    ok('Guardado correctamente.');
                    $('#pass1_datos').val('');
                    $('#pass2_datos').val('');
                break;
            }
        }
    });
}
function agregar_direccion(){
    var id_usuario = sessionStorage.getItem("id_usuario");
    var id_region = $('#region').val();
    var nombre_region = $('#region :selected').text();

    var id_comuna = $('#comuna').val();
    var nombre_comuna = $('#comuna :selected').text();

    var direccion = $('#direccion').val();

    if (id_region == '') {
        error('Debe seleccionar una región.');
        $('#region').focus();
        return;
    }
    if (id_comuna == '') {
        error('Debe seleccionar una comuna.');
        $('#comuna').focus();
        return;
    }
    if (direccion == '') {
        error('Debe ingresar una dirección.');
        $('#direccion').focus();
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'lib/modulo.php',
        data: {
            idfuncion: 5,
            id_usuario: id_usuario,
            id_region: id_region,
            nombre_region: nombre_region,
            id_comuna: id_comuna,
            nombre_comuna: nombre_comuna,
            direccion:direccion
        },
        dataType: 'json',
        success: function(resp) {
            //console.log(resp);
            switch (parseInt(resp.estado)) {
                case 0:
                    error('Se ha producido un error, vuelva a intentarlo más tarde.');
                break;
                case 1:
                    ok('Guardado correctamente.');
                    $('#region').val('');
                    $('#comuna').val('');
                    $('#direccion').val('');
                    $('#direccionModal').modal('hide');
                    get_direcciones();
                break;
            }
        }
    });
}
function get_direcciones(){
    var id_usuario = sessionStorage.getItem("id_usuario");
    $.ajax({
        type: 'POST',
        url: 'lib/modulo.php',
        data: {idfuncion: 7, id_usuario: id_usuario},
        dataType: 'json',
        success: function(resp) {
            //console.log(resp);
            var html = '<thead><tr>'+
                            '<th>Dirección</th>'+
                            '<th>Región</th>'+
                            '<th>Comuna</th>'+
                            '<th>Eliminar</th>'+
                        '</tr></thead>';
            for(var i=0;i<resp.length;i++){
                html+='<tr>'+
                            '<td>'+resp[i].direccion+'</td>'+
                            '<td>'+resp[i].nombre_region+'</td>'+
                            '<td>'+resp[i].nombre_comuna+'</td>'+
                            '<td><button type="button" class="btn btn-danger btn-sm" onclick="open_confirm_direccion('+resp[i].id_direccion+')"><i class="bi bi-trash"></i></button></td>'+
                        '</tr>';
            }

            $('#table_direcciones').html(html);            
        }
    });
}
function eliminar_direccion(id_direccion){
    $.ajax({
        type: 'POST',
        url: 'lib/modulo.php',
        data: {idfuncion: 6, id_direccion: id_direccion},
        dataType: 'json',
        success: function(resp) {
            //console.log(resp);
            switch (parseInt(resp.estado)) {
                case 0:
                    error('Se ha producido un error, vuelva a intentarlo más tarde.');
                break;
                case 1:
                    get_direcciones();
                    $('#confirmModal').modal('hide');
                break;
            }           
        }
    });
}
function open_confirm_direccion(id_direccion){
    $('#btn_confirm_si').attr('onclick', 'eliminar_direccion('+id_direccion+')');
    $('#confirmModal').modal('show');
}
function get_compras(){
    var id_usuario = sessionStorage.getItem("id_usuario");
    $.ajax({
        type: 'POST',
        url: 'lib/modulo.php',
        data: {idfuncion: 16, id_usuario: id_usuario},
        dataType: 'json',
        success: function(resp) {
            //console.log(resp);
            var html = '<thead><tr>'+
                            '<th>Nº orden</th>'+
                            '<th>Detalle</th>'+
                            '<th>Total</th>'+
                            '<th>Estado</th>'+
                            '<th>Fecha</th>'+
                            
                        '</tr></thead>';
            for(var i=0;i<resp.length;i++){
                var prod='<table class="table">';
                var precio_total=0;
                for(var j=0;j<resp[i].productos.length;j++){
                    precio_total += parseInt(resp[i].productos[j].precio_venta); 
                    prod+='<tr>'+
                            '<td>'+resp[i].productos[j].sku+'</td>'+
                            '<td>'+resp[i].productos[j].marca+'</td>'+
                            '<td><a target="_blank" href="detalle_producto/'+resp[i].productos[j].id_producto+'">'+resp[i].productos[j].nombre+'</a></td>'+
                            '<td>'+resp[i].productos[j].cantidad+'</td>'+
                            '<td>'+resp[i].productos[j].precio_venta_format+'</td>'+
                          '</tr>';
                }
                prod+='</table>';

                var iva = precio_total*0.19;
    

                var precio_total_iva = precio_total + iva;

                iva = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'CLP' }).format(iva,);
                precio_total = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'CLP' }).format(precio_total,);
                precio_total_iva = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'CLP' }).format(precio_total_iva,);

                switch(parseInt(resp[i].estado)){
                    case 0:
                        var estado='<span class="badge text-bg-warning">Espera</span>';
                    break;    
                    case 1:
                        var estado='<span class="badge text-bg-success">Aprobado</span>';
                    break; 
                    case 2:
                        var estado='<span class="badge text-bg-danger">Rechazado</span>';
                    break; 
                    case 3:
                        var estado='<span class="badge text-bg-primary">Comprado</span>';
                    break; 
                }
                
                

                html+='<tr>'+
                            '<td>'+resp[i].id_compra+'</td>'+
                            '<td>'+prod+'</td>'+
                            '<td>'+precio_total_iva+'</td>'+
                            '<td>'+estado+'</td>'+
                            '<td>'+resp[i].fecha+'</td>'+
                        '</tr>';
            }

            $('#container_table_compras').html('<table id="table_compras" class="table">'+html+'</table>'); 
            
            $('#table_compras').DataTable( {
                searching: true,
                "lengthChange": false,
                "pageLength": 15,
                "info": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                }
            } );
        }
    });
}
</script>
</head>
<body>
<?php include 'header.php';?>
<div class="container-fluid cont_main bg-F7F7F7">


<div class="container maxw1000 pt40 pb40">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link pointer active" onclick="pestana(1)">Mis datos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link pointer" onclick="pestana(2)">Mis direcciones</a>
        </li>
        <li class="nav-item">
            <a class="nav-link pointer" onclick="pestana(3)">Mis compras</a>
        </li>
    </ul>

    <div class="card auth-card mb-0">
        <div class="card-body">
            <h2 class="mb-3 text-center">Datos personales</h2>
            <form class="plp5 prp5">
                <div class="row">
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="full-name" class="form-label">Nombre</label>
                            <input type="text" class="form-control" id="nombre_datos" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="last-name" class="form-label">Apellido</label>
                            <input type="text" class="form-control" id="apellido_datos" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email_datos" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="phone" class="form-label">Teléfono</label>
                            <input type="text" class="form-control telefono_mask" id="telefono_datos" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <button id="btn-registrase" type="button" class="btn btn-warning" data-loading-text="Enviando..." onclick="actualizar()">Actualizar</button>
                </div>
            </form>
        </div>
        <hr>
        <div class="card-body">
            <h2 class="mb-3 text-center">Cambiar contraseña</h2>
            <form class="plp5 prp5">
                <div class="row">
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="password" class="form-label">Contraseña</label>
                            <input type="password" class="form-control" id="pass1_datos" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="confirm-password" class="form-label">Confirmar contraseña</label>
                            <input type="password" class="form-control" id="pass2_datos" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <button id="btn-registrase" type="button" class="btn btn-warning" data-loading-text="Enviando..." onclick="cambiar_pass()">Cambiar</button>
                </div>
            </form>
        </div>
    </div>

    <div class="card auth-card mb-0" style="display:none">
        <div class="card-body">
            <h2 class="mb-3 text-center">Mis direcciones</h2>
            <button type="button" class="btn btn-success btn-sm d-block ml-auto" data-bs-toggle="modal" data-bs-target="#direccionModal"><i class="bi bi-plus-lg"></i> Agregar</button>
            <table id="table_direcciones" class="table"></table>
        </div>
    </div>

    <div class="card auth-card mb-0" style="display:none">
        <div class="card-body">
            <h2 class="mb-3 text-center">Mis compras</h2>
            <div id="container_table_compras"></div>
        </div>
    </div>


</div>

<!-- Modal DIRECCION-->
<div class="modal fade" id="direccionModal" tabindex="-1" aria-labelledby="direccionModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="direccionModalLabel">Nueva dirección</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <div class="mb-3">
                    <label class="form-label">Región</label>
                    <select id="region" class="form-select"></select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Comuna</label>
                    <select id="comuna" class="form-select"></select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Dirección</label>
                    <input id="direccion" type="text" class="form-control">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-warning" onclick="agregar_direccion()">Agregar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal CONFIRM-->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="confirmModalLabel">Eliminar</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar la dirección?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button id="btn_confirm_si" type="button" class="btn btn-success">Si</button>
      </div>
    </div>
  </div>
</div>

</div>    
<?php include 'footer.php';?>
</body>
</html>