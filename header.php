<nav class="bar-menu">
    <div class="row align-items-center">
        <div class="col-7 col-md-auto">
            <a href="home"><img src="images/logo.svg" class="logo" alt=""></a>
        </div>
        <div class="col-auto none-m">
            <div class="btn_menu"><span>Productos</span></div>
        </div>
        <div class="col-5 col-md-4">
            <div class="input-group">
                <input id="buscador" type="text" class="form-control" placeholder="Busca tu producto" value="<?php echo $_GET['s'];?>">
                <button class="btn btn-outline-secondary bg-FFF" type="button" id="btn-buscar" onclick="buscar_producto()"><i class="bi bi-search"></i></button>
            </div>
        </div>
        <div class="col-4 block-m">
            <div class="btn_menu"><span>Productos</span></div>
        </div>
        <div class="col align-items-end">
            <div class="container-session">
                <div class="row justify-content-end">
                    <div id="container_sesion_nav" class="col-auto">
                        <p class="inicio_sesion"></p>
                    </div>
                    <div class="col-auto">
                        <div class="line-v"></div>
                    </div>
                    <div class="col-auto">
                        <a href="carro" class="cart">
                            <img src="images/cart.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="bg_menu"></div>
<div class="menu">
    <div class="header">
        <button type="button" class="btn-close btn-close-menu" aria-label="Close"></button>
    </div>
    
    <div class="accordion" id="menu_main"></div>
</div>

<div class="bg-popup-login"></div>
<div class="popup-login">
    <button type="button" class="btn-close btn-close-login" aria-label="Close"></button>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card d-flex justify-content-center mb-0 auth-card iq-auth-form">
                <div class="card-body pt40 pb40">                          
                    <h2 class="mb20 text-center">¡Bienvenidos a Cutting Tools Chile SPA!</h2>
                    <form class="plp5 prp5">
                        <div class="row mb20">
                            <div class="col-lg-12 mb10">
                                <div class="form-group">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="email_login" aria-describedby="email" placeholder="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="password" class="form-label">Contraseña</label>
                                    <input type="password" class="form-control" id="password_login" aria-describedby="password" placeholder="">
                                </div>
                            </div>
                            <div class="col-lg-12 d-flex justify-content-between">
                                <div class="form-check"></div>
                                <a href="recuperar">¿Olvidaste tu contraseña?</a>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button id="btn_iniciar_sesion" type="button" class="btn btn-warning" data-loading-text="Ingresando..." onclick="login()">Ingresar</button>
                        </div>

                        <p class="mt-3 text-center">
                            ¿Aún no tienes cuenta? <a href="registrarse" class="text-underline">Haz clic aquí para registrarte.</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>