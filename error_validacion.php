<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>

</head>
<body>
<?php include 'header.php';?>
<div class="container-fluid cont_main bg-F7F7F7">

    <div class="container maxw1000 pt40 pb40">
        <div class="card auth-card  d-flex justify-content-center mb-0">
            <div class="card-body">
                <h2 class="mb-2 text-center">Ups!</h2>
                <p class="text-center">No pudimos validar tu cuenta, intentalo nuevamente.</p>

                <a href="registrarse" class="text-underline mx-auto d-block w-max-content">Haz clic aquí para registrarte.</a>
            </div>
        </div>
    </div>    
</div>    
<?php include 'footer.php';?>
</body>
</html>