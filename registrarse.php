<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>

</head>
<body>
<?php include 'header.php';?>
<div class="container-fluid cont_main bg-F7F7F7">

<div class="container maxw1000 pt40 pb40">
    <div class="card auth-card  d-flex justify-content-center mb-0">
        <div class="card-body">
            <h2 class="mb-2 text-center">Registro</h2>
            <p class="text-center">Ingresa tus datos y disfruta de una experiencia de compra más rápida</p>
            <form class="plp5 prp5">
                <div class="row">
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="full-name" class="form-label">Nombre</label>
                            <input type="text" class="form-control" id="nombre_reg" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="last-name" class="form-label">Apellido</label>
                            <input type="text" class="form-control" id="apellido_reg" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email_reg" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="phone" class="form-label">Teléfono</label>
                            <input type="text" class="form-control telefono_mask" id="telefono_reg" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="password" class="form-label">Contraseña</label>
                            <input type="password" class="form-control" id="pass1_reg" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="confirm-password" class="form-label">Confirmar contraseña</label>
                            <input type="password" class="form-control" id="pass2_reg" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <div class="form-check mb-3">
                            <input type="checkbox" class="form-check-input" id="check_terminos_reg">
                            <label class="form-check-label" for="check_terminos_reg">Acepto los Términos y condiciones</label>
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <button id="btn-registrase" type="button" class="btn btn-warning" data-loading-text="Enviando..." onclick="registrarse()">Registrase</button>
                </div>
                
                <p class="mt-3 text-center">
                    Si ya tienes cuenta <a onclick="open_popup_login()" class="text-underline pointer">Ingresa aquí</a>
                </p>
            </form>
        </div>
    </div>
</div>

</div>    
<?php include 'footer.php';?>
</body>
</html>