$(document).ready(function() {

	$('.btn_menu').click(function(){
		$('.bg_menu').css('width','100%');
		$('.menu').css('width','80%');
	});
	$('.btn-close-menu').click(function(){
		$('.menu').css('width','0%');
		$('.bg_menu').css('width','0%');
	});
	
	$('.btn-close-login').click(function(){
		$('.popup-login').fadeOut(200);
		$('.bg-popup-login').fadeOut(200);
	});

	Fancybox.bind("[data-fancybox]", {
		// Your custom options
	});
});
function open_popup_login(){
	$('.popup-login').fadeIn(200);
	$('.bg-popup-login').fadeIn(200);
}