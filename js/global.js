var txt_init='';
var cart_global=[];
$(document).ready(function() {
	$('a[href*="#"]:not([href="#"])').click(function () {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
			$('.container-menu').css('width','0%');
			$('.btn-menu').removeClass('open');
            $('html, body').animate({
                scrollTop: target.offset().top - 114
            }, 100);
            return false;
        }
    });
	$('.btn-menu').click(function(){
		$(this).toggleClass('open');
		if($(this).hasClass("open")){
			$('.container-menu').css('width','90%');
		}else{
			$('.container-menu').css('width','0%');
		}
	});

	$('.telefono_mask').mask('(+56) 0 0000 0000');
    $(".rut_mask").mask("00.000.000-h", { reverse: true, translation: { "h": { pattern: /[0-9|k]/, optional: false } } });


	if(sessionStorage.getItem("cart")!==null){
		cart_global = JSON.parse(sessionStorage.getItem('cart'));
	}

	$("#buscador").on("keydown",function search(e) {
		if(e.keyCode == 13) {
			buscar_producto();
		}
	});

	set_nav_login();
	set_menu();
});
function buscar_producto(){
	var buscar = $("#buscador").val();
	if(buscar!=''){
		top.location.href="productos?s="+buscar;
	}
}
function set_menu(){
	$.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:10},
		dataType: 'json',
		success: function(a){
			var html='';
			for(var i=0;i<a.length;i++){
				if(a[i].subcategorias.length>0){
					var html_sub='';
					for(var j=0;j<a[i].subcategorias.length;j++){
						html_sub+='<li class="list-group-item"><a href="productos?cat='+a[i].id_categoria+'&sub='+a[i].subcategorias[j].id_subcategoria+'" class="color-000">'+a[i].subcategorias[j].nombre_subcategoria+'</a></li>';
					}
					html+='<div class="accordion-item">'+
								'<h2 class="accordion-header">'+
									'<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#menucat_'+a[i].id_categoria+'" aria-expanded="false" aria-controls="menucat_'+a[i].id_categoria+'">'+a[i].nombre_categoria+'</button>'+
								'</h2>'+
								'<div id="menucat_'+a[i].id_categoria+'" class="accordion-collapse collapse" data-bs-parent="#menu_main">'+
									'<div class="accordion-body">'+
										'<ul class="list-group list-group-flush">'+html_sub+'</ul>'+
									'</div>'+
								'</div>'+
							'</div>';
				}else{
					html+='<div class="accordion-item">'+
								'<h2 class="accordion-header">'+
								'<a href="productos?cat='+a[i].id_categoria+'" class="link_menu">'+a[i].nombre_categoria+'</a>'+
								'</h2>'+
							'</div>';
				}
			}
			$('#menu_main').html(html);
        },
        error: function(){
            
        }
    });
}
function set_nav_login(){
	if(sessionStorage.getItem("id_usuario")!==null){
		$('#container_sesion_nav').html('<p><a href="micuenta" class="color-FFF">Hola, '+sessionStorage.getItem("nombre_usuario")+'</a> &nbsp;&nbsp;<span title="cerrar sesión" class="size18 text-danger" onclick="cerrar_sesion()"><i class="bi bi-box-arrow-right"></i></span></p>');
	}else{
		$('#container_sesion_nav').html('<p class="inicio_sesion" onclick="open_popup_login()">Inicia sesión</p>');
	}
}
function cerrar_sesion(){
	sessionStorage.removeItem("id_usuario");
	sessionStorage.removeItem("email_usuario");
	sessionStorage.removeItem("nombre_usuario");
	sessionStorage.removeItem("apellido_usuario");
	sessionStorage.removeItem("telefono_usuario");
	top.location.href="home";
}
function registrarse(){
	var nombre = $('#nombre_reg').val();
	var apellido = $('#apellido_reg').val();
    var email = $('#email_reg').val();
    var telefono = $('#telefono_reg').val();
    var pass1 = $('#pass1_reg').val();
    var pass2 = $('#pass2_reg').val();
    var check = $('#check_terminos_reg').prop('checked');
    
    if(nombre==''){
        error('Debe ingresar el nombre');
        $('#nombre_reg').focus();
        return;
    }
	if(apellido==''){
        error('Debe ingresar el apellido');
        $('#apellido_reg').focus();
        return;
    }
    if(email==''){
        error('Debe ingresar el email');
        $('#email_reg').focus();
        return;
    }
    if(!validarEmail(email)){
        error('Debe ingresar un email válido');
        $('#email_reg').focus();
        return;
    }
	if(telefono==''){
        error('Debe ingresar el teléfono');
        $('#telefono_reg').focus();
        return;
    }
	if(pass1==''){
        error('Debe ingresar la contraseña');
        $('#pass1_reg').focus();
        return;
    }
	if(pass1!=pass2){
        error('La contraseña no coincide');
        $('#pass2_reg').focus();
        return;
    }
	if(!check){
        error('Debes aceptar los términos y condiciones');
        return;
    }
	btn_enviando('btn-registrase');
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:1, nombre:nombre, apellido:apellido, telefono:telefono, email:email, pass1:pass1},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, la información no ha sido enviada.');
				break;
				case 1:
					ok('Revise su correo para completar el registro');
					$('#nombre_reg').val('');
					$('#apellido_reg').val('');
					$('#telefono_reg').val('');
					$('#email_reg').val('');
					$('#pass1_reg').val('');
					$('#pass2_reg').val('');
				break;
				case 2:
					error('Ya existe una cuenta con este correo.');
				break;
			}
            btn_reset('btn-registrase');
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
            btn_reset('btn-registrase');
        }
    });
}
function login(){
	var user = $('#email_login').val();
    var pass = $('#password_login').val();
    
    if(user==''){
        error('Debe ingresar el email');
        $('#email_login').focus();
        return;
    }
	if(pass==''){
        error('Debe ingresar la contraseña');
        $('#password_login').focus();
        return;
    }
	btn_enviando('btn_iniciar_sesion');
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:2, user:user, pass:pass},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, la información es inválida.');
				break;
				case 1:
					sessionStorage.setItem("id_usuario", a.id_usuario);
					sessionStorage.setItem("email_usuario", a.email_usuario);
					sessionStorage.setItem("nombre_usuario", a.nombre_usuario);
					sessionStorage.setItem("apellido_usuario", a.apellido_usuario);
					sessionStorage.setItem("telefono_usuario", a.telefono_usuario);

					location.reload();
				break;
				case 2:
					error('Completa el proceso de activación de tu cuenta.');
				break;
			}
            btn_reset('btn_iniciar_sesion');
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
            btn_reset('btn_iniciar_sesion');
        }
    });
}
function recuperar(){
	var email = $('#email_rec').val();
	$.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:8, email:email},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, El email no se encuentra registrado.');
				break;
				case 1:
					ok('Revise su correo para continuar el proceso');
					$('#email_rec').val('');
				break;
				case 2:
					error('<strong>Error</strong>, la información no ha sido enviada.');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function guardar_pass(){
    var id = $('#id_hidden').val();
    var pass1 = $('#pass1_reg').val();
    var pass2 = $('#pass2_reg').val();
    
	if(pass1==''){
        error('Debe ingresar la contraseña');
        $('#pass1_reg').focus();
        return;
    }
	if(pass1!=pass2){
        error('La contraseña no coincide');
        $('#pass2_reg').focus();
        return;
    }
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:9, clave:pass1, id:id},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, la información no ha sido cambiada.');
				break;
				case 1:
					ok('Contraseña cambiada correctamente');
					$('#pass1_reg').val('');
					$('#pass2_reg').val('');
					setTimeout(function(){
						top.location.href="home";
					},4000);
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function add_cart(){
	var producto = {
		id: $('#id_hidden').val(),
		nombre: $('#nombre_hidden').val(),
		marca: $('#marca_hidden').val(),
		precio: $('#precio_hidden').val(),
		precio_num: parseInt($('#precionum_hidden').val()),
		sku: $('#sku_hidden').val(),
		imagen: $('#imagen_hidden').val(),
		cantidad: parseInt($('#cant_cart').val())
	};
	var indice = -1;
	for(var i=0;i<cart_global.length;i++){
		if(cart_global[i].id==producto.id){
			indice = i;
		}
	}
	if(indice==-1){
		cart_global.push(producto);
	}else{
		cart_global[indice].cantidad= parseInt(cart_global[indice].cantidad) + parseInt(producto.cantidad);
	}
	sessionStorage.setItem("cart", JSON.stringify(cart_global));
	ok('Producto agregado a tu carro');
}



function enviar(){
    var nombre = $('#nombre').val();
    var comentario = $('#comentario').val();
    var telefono = $('#telefono').val();
    var email = $('#email').val();
    if(nombre==''){
        error('Debe ingresar el nombre');
        $('#nombre').focus();
        return;
    }
    if(telefono==''){
        error('Debe ingresar el teléfono');
        $('#telefono').focus();
        return;
    }
    if(email==''){
        error('Debe ingresar el email');
        $('#email').focus();
        return;
    }
    if(!validarEmail(email)){
        error('Debe ingresar un email válido');
        $('#email').focus();
        return;
    }

    btn_enviando();
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:1, nombre:nombre, comentario:comentario, telefono:telefono, email:email},
		dataType: 'json',
		success: function(a){
            if(a.estado==1){
                ok('Enviado correctamente');
                $('#nombre').val('');
                $('#comentario').val('');
                $('#telefono').val('');
                $('#email').val('');
            }else{
                error('<strong>Error</strong>, la información no ha sido enviada.');
            }
            btn_reset();
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
            btn_reset();
        }
    });
}
function error(text){
    alertify.error(text);
}
function ok(text){
    alertify.success(text);
}
function btn_enviando(btn){
    $('#'+btn).prop('disabled', true);
    txt_init = $('#'+btn).text();
    var txt_loading = $('#'+btn).attr('data-loading-text');
    $('#'+btn).html(txt_loading);
}
function btn_reset(btn){
    $('#'+btn).prop('disabled', false);
    $('#'+btn).html(txt_init);
}

function revisarDigito( dvr ){	
	dv = dvr + ""	
	if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')	{		
		error("Debe ingresar un dígito verificador válido");		
		//$('#rut').focus();		
		//$('#rut').select();		
		return false;	
	}	
	return true;
}
function revisarDigito2( crut ){	
	largo = crut.length;	
	if ( largo < 2 ){		
		error("Debe ingresar el Rut completo")		
		//$('#rut').focus();		
		//$('#rut').select();		
		return false;	
	}	
	if ( largo > 2 )		
		rut = crut.substring(0, largo - 1);	
	else		
		rut = crut.charAt(0);	
	dv = crut.charAt(largo-1);	
	revisarDigito( dv );	

	if ( rut == null || dv == null )
		return 0	

	var dvr = '0'	
	suma = 0	
	mul  = 2	

	for (i= rut.length -1 ; i >= 0; i--){	
		suma = suma + rut.charAt(i) * mul		
		if (mul == 7)			
			mul = 2		
		else    			
			mul++	
	}	
	res = suma % 11	
	if (res==1)		
		dvr = 'k'	
	else if (res==0)		
		dvr = '0'	
	else	
	{		
		dvi = 11-res		
		dvr = dvi + ""	
	}
	if ( dvr != dv.toLowerCase() ){		
		error("EL Rut es incorrecto");		
		//$('#rut').focus();		
		//$('#rut').select();		
		return false	
	}
	return true
}
function Rut(texto){
	var tmpstr = "";	
	for ( i=0; i < texto.length ; i++ )		
		if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
			tmpstr = tmpstr + texto.charAt(i);	
	texto = tmpstr;	
	largo = texto.length;	

	if ( largo < 2 ){		
		error("Debe ingresar el RUT completo");		
		//$('#rut').focus();		
		//$('#rut').select();		
		return false;	
	}	

	for (i=0; i < largo ; i++ ){			
		if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" ){			
			error("El valor ingresado no corresponde a un RUT válido");			
			//$('#rut').focus();			
			//$('#rut').select();			
			return false;		
		}	
	}	

	var invertido = "";	
	for ( i=(largo-1),j=0; i>=0; i--,j++ )		
		invertido = invertido + texto.charAt(i);	
	var dtexto = "";	
	dtexto = dtexto + invertido.charAt(0);	
	dtexto = dtexto + '-';	
	cnt = 0;	

	for ( i=1,j=2; i<largo; i++,j++ ){		
		//error("i=[" + i + "] j=[" + j +"]" );		
		if ( cnt == 3 ){			
			dtexto = dtexto + '.';			
			j++;			
			dtexto = dtexto + invertido.charAt(i);			
			cnt = 1;		
		}else{				
			dtexto = dtexto + invertido.charAt(i);			
			cnt++;		
		}	
	}	

	invertido = "";	
	for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )		
		invertido = invertido + dtexto.charAt(i);	

	//$('#rut').value = invertido.toUpperCase()		

	if ( revisarDigito2(texto) )		
		return true;	

	return false;
}
function validarEmail(valor) {
  if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor)){
   	return true;
  } else {
    return false;
  }
}