<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>
<script>
$(document).ready(function() {  
    show_cart();
});
function show_cart(){
    if(cart_global.length==1){
        var html='<p class="color-000 size20 bold">Carro (1 producto)</p>';
    }else{
        var html='<p class="color-000 size20 bold">Carro ('+cart_global.length+' productos)</p>';
    }
    var precio_total = 0;
    if(cart_global.length==0){
        html+='<div class="row bg-FFF mb10 pl20 pr20"><h4 class="pt40 pb40 text-center">Tu Carro está vacío</h4></div>';
    }
    for(var i=0;i<cart_global.length;i++){
        precio_total += parseInt(cart_global[i].precio_num) * parseInt(cart_global[i].cantidad);
		html+='<div class="row bg-FFF mb10 pl20 pr20">'+
                    '<div class="col-lg-3 d-flex align-items-center">'+
                        '<img src="images/producto/'+cart_global[i].imagen+'" alt="" class="img_main_producto"></a>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="detalle-producto">'+
                            '<div class="box-info">'+
                                '<p class="marca">'+cart_global[i].marca+'</p>'+
                                '<p class="nombre_producto">'+cart_global[i].nombre+'</p>'+
                                '<div class="line"></div>'+
                                '<p class="sku">SKU: <b>'+cart_global[i].sku+'</b></p>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-3">'+
                        '<div class="detalle-producto">'+
                            '<div class="box-info">'+
                                '<p class="precio_venta">$'+cart_global[i].precio+'</p>'+
                                '<div class="row g-3 align-items-center mb0 mt10">'+
                                    '<div class="col-auto">'+
                                        '<label class="col-form-label col-form-label-sm color-707070">Cantidad</label>'+
                                    '</div>'+
                                    '<div class="col-auto">'+
                                        '<input id="cant_'+cart_global[i].id+'" onchange="update_prod_cart('+cart_global[i].id+')" type="number" class="form-control form-control-sm maxw70" min="1" value="'+cart_global[i].cantidad+'">'+
                                    '</div>'+
                                '</div>'+
                                '<button type="button" class="btn btn-link text-danger ml-auto d-block" onclick="open_eliminar_cart('+cart_global[i].id+')"><i class="bi bi-trash"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
	}
    var iva = precio_total*0.19;
    

    var precio_total_iva = precio_total + iva;

    iva = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'CLP' }).format(iva,);
    precio_total = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'CLP' }).format(precio_total,);
    precio_total_iva = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'CLP' }).format(precio_total_iva,);
    $('#container_cart').html(html);
    $('#total_precio_productos').html('$'+precio_total);
    $('#costo_iva').html('$'+iva);
    $('#total_compra').html('$'+precio_total_iva);
} 
function update_prod_cart(id){

	for(var i=0;i<cart_global.length;i++){
		if(cart_global[i].id==id){
			cart_global[i].cantidad = $('#cant_'+id).val();
		}
	}
    
    sessionStorage.setItem("cart", JSON.stringify(cart_global));
    show_cart();
} 
function eliminar_cart(id){
    var indice = -1;
	for(var i=0;i<cart_global.length;i++){
		if(cart_global[i].id==id){
			indice = i;
		}
	}
    if(indice>-1){
        cart_global.splice(indice, 1);
    }
    sessionStorage.setItem("cart", JSON.stringify(cart_global));
    show_cart();
    $('#confirmModal').modal('hide');
    ok('Producto eliminado de tu carro');
}  
function open_eliminar_cart(id_direccion){
    $('#btn_confirm_si').attr('onclick', 'eliminar_cart('+id_direccion+')');
    $('#confirmModal').modal('show');
}

function guardar_carro(){
    var id_usuario = sessionStorage.getItem("id_usuario");
    if(id_usuario===null){
        open_popup_login();
    }else{
        var cart = JSON.parse(sessionStorage.getItem("cart"));
        if(cart===null){
            error('Tu carrito se encuentra vacío.');
            return
        }
        $.ajax({
            type: 'POST',
            url: 'lib/modulo.php',
            data: {idfuncion:15, cart:cart, id_usuario:id_usuario},
            dataType: 'json',
            success: function(a){
                switch(a.estado){
                    case 0:
                        error('<strong>Error</strong>, la información no fue enviada.');
                    break;
                    case 1:
                        ok('Solicitud enviada');
                        sessionStorage.removeItem("cart");
                        cart_global=[];
                        top.location.href="compra";
                    break;
                }
            },
            error: function(){
                
            }
        }); 
    }
}
</script>
</head>
<body>
<?php include 'header.php';?>
<div class="container-fluid cont_main pb30 bg-F7F7F7">
    <div class="container pt30 mt40">
        <div class="row ml0 mr0">
            <div id="container_cart" class="col-md-9"></div>
            <div class="col-md-3">
                <p class="color-000 size20 bold">Resumen de la compra</p>
                <div class="box_resumen_compra">
                    <table class="table size14">
                        <tr>
                            <td>Productos (2)</td>
                            <td id="total_precio_productos" align="right">$0</td>
                        </tr>
                        <tr>
                            <td>Costo envío</td>
                            <td id="costo_envio" align="right">$0</td>
                        </tr>
                        <tr>
                            <td>IVA</td>
                            <td id="costo_iva" align="right">$0</td>
                        </tr>
                        <tr>
                            <td><b>Total</b></td>
                            <td id="total_compra" align="right"><b>$0</b></td>
                        </tr>
                    </table>

                    <button onclick="guardar_carro()" type="button" class="btn btn-warning btn-mod mt30">Solicitar compra</button>
                </div>
            </div>
        </div>


    </div>

</div>    

<!-- Modal CONFIRM-->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="confirmModalLabel">Eliminar</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar el producto?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button id="btn_confirm_si" type="button" class="btn btn-success">Si</button>
      </div>
    </div>
  </div>
</div>
<?php include 'footer.php';?>
</body>
</html>