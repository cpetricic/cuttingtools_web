<?php
$cap = explode('/',$_SERVER['REQUEST_URI']);
$_GET['i'] = $cap[2];
include('lib/conex.php');
$query="SELECT id_producto,
                id_grupo,
                id_categoria,
                id_subcategoria,
                sku_producto,
                nombre_producto,
                marca_producto,
                precio_venta_producto,
                precio_normal_producto,
                descripcion_producto,
                orden,
                estado,
                fecha_ingreso
        FROM producto
        WHERE estado=1 AND id_producto=".$_GET['i'];  

$result=mysql_query($query,$link);
$cant=mysql_num_rows($result);
if($cant==0){
    header('Location: '.$ruta);
    die();
}
$row=mysql_fetch_array($result);
$query2="SELECT id_categoria, nombre_categoria FROM categoria WHERE id_categoria=".$row['id_categoria'];
$result2=mysql_query($query2,$link);
$row2=mysql_fetch_array($result2);

if($row['id_subcategoria']>0){
    $query3="SELECT id_subcategoria, nombre_subcategoria FROM subcategoria WHERE id_subcategoria=".$row['id_subcategoria'];
    $result3=mysql_query($query3,$link);
    $row3=mysql_fetch_array($result3);
    $id_subcategoria = utf8_encode($row3['id_subcategoria']);
    $nombre_subcategoria = utf8_encode($row3['nombre_subcategoria']);

    //productos?cat='+a[i].id_categoria+'&sub='+a[i].subcategorias[j].id_subcategoria
    $breadcrumb='<li class="breadcrumb-item"><a class="color-FFBC07" href="productos?cat='.utf8_encode($row2['id_categoria']).'">'.utf8_encode($row2['nombre_categoria']).'</a></li>
                 <li class="breadcrumb-item"><a class="color-FFBC07" href="productos?cat='.utf8_encode($row2['id_categoria']).'&sub='.$id_subcategoria.'">'.$nombre_subcategoria.'</a></li>';
}else{
    $id_subcategoria = 0;
    $nombre_subcategoria = '';

    $breadcrumb='<li class="breadcrumb-item"><a class="color-FFBC07" href="productos?cat='.utf8_encode($row2['id_categoria']).'">'.utf8_encode($row2['nombre_categoria']).'</a></li>';
}
$imagenes=array();
$query3="SELECT nombre_imagen_producto FROM imagen_producto WHERE id_producto=".$row['id_producto']." ORDER BY orden ASC";
$result3=mysql_query($query3,$link);
$cant3=mysql_num_rows($result3);
if($cant3>0){
    while($row3=mysql_fetch_array($result3)){
        $imagenes[] = utf8_encode($row3['nombre_imagen_producto']);
    }
}else{
    $imagenes[] = 'no-picture.jpg';
}

$datos = array(
    'id_producto'           => utf8_encode($row['id_producto']),
    'id_categoria'          => utf8_encode($row2['id_categoria']), 
    'nombre_categoria'      => utf8_encode($row2['nombre_categoria']), 
    'id_subcategoria'       => $id_subcategoria, 
    'nombre_subcategoria'   => $nombre_subcategoria, 
    'sku'                   => utf8_encode($row['sku_producto']),
    'nombre'                => utf8_encode($row['nombre_producto']),
    'marca'                 => utf8_encode($row['marca_producto']),
    'precio_venta'          => number_format($row['precio_venta_producto'],0,',','.'),
    'precio_venta_num'      => $row['precio_venta_producto'],
    'precio_normal'         => number_format($row['precio_normal_producto'],0,',','.'),
    'descripcion'           => utf8_encode($row['descripcion_producto']),
    'orden'                 => utf8_encode($row['orden']),
    'estado'                => utf8_encode($row['estado']), 
    'fecha'                 => date_format(date_create($row['fecha_ingreso']), 'd-m-Y H:i:s')
);

if($row['id_grupo']>0){
    $query5="SELECT id_producto,
                id_grupo,
                id_categoria,
                id_subcategoria,
                sku_producto,
                nombre_producto,
                marca_producto,
                precio_venta_producto,
                precio_normal_producto,
                descripcion_producto,
                orden,
                estado,
                fecha_ingreso
        FROM producto
        WHERE estado=1 AND id_grupo=".$row['id_grupo'];  
    $result5=mysql_query($query5,$link);
    $opt_grupo='';
    while($row5=mysql_fetch_array($result5)){
        if($_GET['i']==$row5['id_producto']){
            $opt_grupo .= '<option value="'.$row5['id_producto'].'" selected>'.utf8_encode($row5['sku_producto']).' - '.utf8_encode($row5['nombre_producto']).'</option>';
        }else{
            $opt_grupo .= '<option value="'.$row5['id_producto'].'">'.utf8_encode($row5['sku_producto']).' - '.utf8_encode($row5['nombre_producto']).'</option>';
        }
        
    }
    
    $grupo_html='<select id="select_grupo" class="form-select" onchange="cambio_producto()">'.$opt_grupo.'</select>';
}else{
    $grupo_html='';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>
<script>
function cambio_producto(){
    var id=$('#select_grupo').val();    
    top.location.href="detalle_producto/"+id;
}    
</script>
</head>
<body>
<input type="hidden" id="id_hidden" value="<?php echo $_GET['i'];?>">
<input type="hidden" id="nombre_hidden" value="<?php echo $datos['nombre']?>">
<input type="hidden" id="marca_hidden" value="<?php echo $datos['marca']?>">
<input type="hidden" id="precio_hidden" value="<?php echo $datos['precio_venta']?>">
<input type="hidden" id="precionum_hidden" value="<?php echo $datos['precio_venta_num']?>">
<input type="hidden" id="sku_hidden" value="<?php echo $datos['sku']?>">
<input type="hidden" id="imagen_hidden" value="<?php echo $imagenes[0]?>">
<?php include 'header.php';?>
<div class="container-fluid cont_main pb30 bg-F7F7F7">

    <div class="container plp5 prp5 pt30 mt40 bg-FFF">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="color-FFBC07" href="home">Home</a></li>
                <?php echo $breadcrumb;?>
            </ol>
        </nav>
        <div class="row">
            <div class="col-lg-5 mb20">
                <a href="images/producto/<?php echo $imagenes[0];?>" data-fancybox="gallery"><img src="images/producto/<?php echo $imagenes[0];?>" alt="" class="img_main_producto"></a>
                <div class="d-none">
                    <?php
                    for($i=1;$i<count($imagenes);$i++){
                        echo '<a href="images/producto/'.$imagenes[$i].'" data-fancybox="gallery"></a>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="detalle-producto">
                    <div class="box-info">
                        <p class="marca"><?php echo $datos['marca'];?></p>
                        <p class="nombre_producto"><?php echo $datos['nombre'];?></p>
                        <div class="line"></div>
                        <p class="sku">SKU: <b><?php echo $datos['sku'];?></b></p>

                        <p class="precio_venta">$<?php echo $datos['precio_venta'];?></p>
                        <p class="precio_normal">NORMAL: <b>$<?php echo $datos['precio_normal'];?></b></p>

                        <?php echo $grupo_html;?>                  
                        <div class="row g-3 align-items-center mb10 mt30">
                            <div class="col-auto">
                                <label class="col-form-label col-form-label-sm color-707070">Cantidad</label>
                            </div>
                            <div class="col-auto">
                                <input id="cant_cart" type="number" class="form-control form-control-sm maxw70" min="1" value="1">
                            </div>
                        </div>
                        <button type="button" class="btn btn-warning btn-mod" onclick="add_cart()">Agregar al carro</button>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>

    <div class="container plp5 prp5 pt40 pb40 mt10 bg-FFF">
        <div class="row">
            <div class="col-12">
                <p class="titulo_detalle_producto">Especificaciones</p>

                <?php echo $datos['descripcion'];?>
            </div>
        </div>
    </div>
</div>    
<?php include 'footer.php';?>
</body>
</html>