<base href="https://localhost/cuttingtools_web/"/>
<!-- <base href="https://cuttingtools.cl/"/> -->

<script src="js/jquery-3.7.1.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-icons/font/bootstrap-icons.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

<script src="js/mask/jquery.mask.min.js"></script>
<script src="js/alert/alertify.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="js/alert/alertify.core.css">
<link rel="stylesheet" type="text/css" href="js/alert/alertify.default.css">

<link rel="stylesheet" href="js/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="js/owlcarousel/owl.theme.default.min.css">
<script src="js/owlcarousel/owl.carousel.min.js"></script>

<script src="js/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="js/fancybox/fancybox.css"/>

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="css/fonts.min.css">
<link rel="stylesheet" href="css/custom.min.css">
<link rel="stylesheet" href="css/estilo.min.css">
<link rel="stylesheet" href="css/media.min.css">

<!-- <link rel="stylesheet" href="css/main.min.css"> -->

<script src="js/global.js"></script>
<script src="js/menu.js"></script>