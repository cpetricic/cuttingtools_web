<div class="container-fluid cont-footer">
    <div class="mx-auto maxwp90">
        <div class="row">
            <div class="col-md-12 col-lg">
                <div class="maxw370">
                    <a href="home"><img src="images/logo.svg" class="logo mb40" alt=""></a>

                    <p class="size14 color-FFF mb5">Suscribete y entérate de las ultimas novedades</p>
                    <div class="input-group mb40">
                        <input type="text" class="form-control" placeholder="Ingresa tu email">
                        <button class="btn btn-outline-secondary bg-FFF" type="button" id="button-suscribete">Enviar</button>
                    </div>

                    <p class="size14 color-FFF mb5">Medios de pago</p>
                    <img src="images/webpay.svg" class="mb20" alt="">
                </div>
            </div>
            <div class="col-auto none-m"><div class="vline-footer"></div></div>
            <div class="col-md-12 col-lg pt20">
                <p class="size14 color-FFF">Tienda</p>
                <a href="https://maps.app.goo.gl/52v3XpdegMYCgRyN6" target="_blank"><p class="color-C6C6C6 size15">Alcalde Pedro Alarcón 765, San Miguel, Región Metropolitana </p></a>
            </div>
            <div class="col-auto"><div class="vline-footer"></div></div>
            <div class="col-md-12 col-lg pt20">
                <a href="#" target="_blank"><p class="size14 color-FFF">Políticas</p></a>
                <a href="#" target="_blank"><p class="color-C6C6C6 size15">Términos y condiciones</p></a>
                <a href="#" target="_blank"><p class="color-C6C6C6 size15">Políticas de cambio y devolución</p></a>
                <a href="#" target="_blank"><p class="color-C6C6C6 size15">Condiciones de retiro y despacho</p></a>
                <a href="#" target="_blank"><p class="color-C6C6C6 size15 mb30">Politicas de privacidad</p></a>
            </div>
            <div class="col-auto none-m"><div class="vline-footer"></div></div>
            <div class="col-md-12 col-lg">
                <p class="size14 color-FFF mb5">Call Center</p>
                
                <a href="tel:+56 2 2552 1181"><p class="color-C6C6C6 size15 mb5"><i class="bi bi-telephone-fill"></i> +56 2 2552 1181</p></a>
                <a href="tel:+56 2 2553 6944"><p class="color-C6C6C6 size15 mb5"><i class="bi bi-telephone-fill"></i> +56 2 2553 6944</p></a>

                <p class="size14 color-FFF mt20 mb5">Whatsapp</p>
                <a href="https://wa.me/56985019080" target="_blank"><p class="color-C6C6C6 size15 mb5"><i class="bi bi-whatsapp"></i> +56 9 8501 9080</p></a>

                <p class="size14 color-FFF mt20 mb5">Email</p>
                <a href="mailto:ventas@cuttingtools.cl"><p class="color-C6C6C6 size15 mb5"><i class="bi bi-envelope"></i> ventas@cuttingtools.cl</p></a>
                
            </div>
        </div>
    </div>
</div>