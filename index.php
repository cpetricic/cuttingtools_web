<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>
<script>
$(document).ready(function(){ 
    $("#carusel-marcas").owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:false,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            }
        }
    });
    get_slider_home();
    get_banner_home();
    get_destacados();
});
function get_slider_home(){
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:12},
		dataType: 'json',
		success: function(a){
            var html='';      
			for(var i=0;i<a.length;i++){
                if(a[i].url==''){
                    html+='<div class="item"><img src="images/home/slider/'+a[i].imagen+'" alt=""></div>';
                }else{
                    html+='<div class="item"><a href="'+a[i].url+'"><img src="images/home/slider/'+a[i].imagen+'" alt=""></a></div>';
                }
                
            }
            $('#carusel-home').html(html);
            $("#carusel-home").owlCarousel({
                loop:true,
                margin:0,
                items:1,
                nav:true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true
            }); 
        },
        error: function(){
            
        }
    });    
}  
function get_banner_home(){
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:13},
		dataType: 'json',
		success: function(a){
            for(var i=0;i<a.length;i++){
                $('#banner_pos'+a[i].posicion).css('background-image', 'url(images/home/banner/'+a[i].imagen+')');
                if($.trim(a[i].url)!=''){
                    $('#banner_pos'+a[i].posicion).attr('href', a[i].url);    
                }
            }
        },
        error: function(){
            
        }
    });    
}    
function get_destacados(){
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:14},
		dataType: 'json',
		success: function(a){
            var html="";
            if(a.length==0){
                $('#container_destacados').hide();
            }
            for(var i=0;i<a.length;i++){
                html+='<div class="item">'+
                            '<a href="detalle_producto/'+a[i].id_producto+'" class="box-destacado">'+
                                '<div class="img-producto" style="background-image: url(images/producto/'+a[i].imagen+')"></div>'+
                                '<div class="box-info">'+
                                    '<p class="marca">'+a[i].marca+'</p>'+
                                    '<p class="nombre_producto">'+a[i].nombre+'</p>'+
                                    '<div class="line"></div>'+
                                    '<p class="sku">SKU: <b>'+a[i].sku+'</b></p>'+
                                    '<p class="precio_venta">$'+a[i].precio_venta+'</p>'+
                                    '<p class="precio_normal">NORMAL: <b>$'+a[i].precio_normal+'</b></p>'+
                                '</div>'+
                            '</a>'+
                        '</div>';
            }
            $('#carusel-destacados').html(html);
            $("#carusel-destacados").owlCarousel({
                loop:true,
                margin:20,
                nav:false,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            });
        },
        error: function(){
            
        }
    });    
}  
</script>
</head>
<body>
<?php include 'header.php';?>
<div class="container-fluid cont_main">

<div class="container-fluid pl0 pr0">
    <div id="carusel-home" class="owl-carousel owl-theme"></div> 
</div>

<div class="container-fluid pt40 plp4 prp4">
    <div class="row pl20 pr20">
        <div class="col-md-6">
            <a id="banner_pos1" class="box-fila2"></a>
        </div>
        <div class="col-md-6">
            <a id="banner_pos2" class="box-fila2"></a>
        </div>
    </div>
</div> 

<div class="container-fluid pt40 plp4 prp4">
    <div class="row pl20 pr20">
        <div class="col-md-3">
            <a id="banner_pos3" class="box-fila3"></a>
        </div>
        <div class="col-md-3">
            <a id="banner_pos4" class="box-fila3"></a>
        </div>
        <div class="col-md-3">
            <a id="banner_pos5" class="box-fila3"></a>
        </div>
        <div class="col-md-3">
            <a id="banner_pos6" class="box-fila3"></a>
        </div>
    </div>
</div> 

<div id="container_destacados" class="container-fluid pt40 plp5 prp5">
    <h2 class="titulo1 mb20">Productos <b>destacados</b></h2>
    <div id="carusel-destacados" class="owl-carousel owl-theme"></div> 
</div> 


<div class="container-fluid pt40 plp5 prp5">
    <h2 class="titulo1 mb20">Nuevas <b>promociones</b></h2>

    <div class="row">
        <div class="col-md-6">
            <a id="banner_pos7" class="box-promo1"></a>
        </div>
        <div class="col-md-6">
            <a id="banner_pos8" class="box-promo2"></a>
            <a id="banner_pos9" class="box-promo3"></a>
        </div>
    </div>
</div>

<div class="container-fluid pt60 pb60 plp5 prp5">
    <div class="container maxw1000">
        <div id="carusel-marcas" class="owl-carousel owl-theme">
            <div class="item">
                <div class="box_marca" style="background-image: url(images/marcas/marca1.jpg)"></div>
            </div>
            <div class="item">
                <div class="box_marca" style="background-image: url(images/marcas/marca2.jpg)"></div>
            </div>
            <div class="item">
                <div class="box_marca" style="background-image: url(images/marcas/marca3.jpg)"></div>
            </div>
        </div>
    </div>
</div>

</div>  
<?php include 'footer.php';?>
</body>
</html>