<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>

</head>
<body>
<?php include 'header.php';?>
<div class="container-fluid cont_main bg-F7F7F7">

<div class="container maxw1000 pt40 pb40">
    <div class="card auth-card  d-flex justify-content-center mb-0">
        <div class="card-body">
            <h2 class="mb-2 text-center">Recuperar contraseña</h2>
            <p class="text-center">Ingresa tu email y continua con los pasos desde tu correo</p>
            <form class="plp5 prp5">
                <div class="row justify-content-center">
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email_rec" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <button id="btn-recuperar" type="button" class="btn btn-warning" data-loading-text="Enviando..." onclick="recuperar()">Recuperar</button>
                </div>
                
                <p class="mt-3 text-center">
                    Si ya tienes cuenta <a onclick="open_popup_login()" class="text-underline pointer">Ingresa aquí</a>
                </p>
            </form>
        </div>
    </div>
</div>

</div>    
<?php include 'footer.php';?>
</body>
</html>