
<?php
include('lib/conex.php');
$query="SELECT id_usuario, nombre_usuario, apellido_usuario, email_usuario, telefono_usuario, estado FROM usuario WHERE estado=2 AND id_usuario='".$_GET['i']."'";
$result=mysql_query($query,$link);
$cant=mysql_num_rows($result);
if($cant==0){
    header("Location: ".$ruta);
    die();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>

</head>
<body>
<?php include 'header.php';?>
<div class="container-fluid cont_main bg-F7F7F7">

<div class="container maxw1000 pt40 pb40">
    <div class="card auth-card  d-flex justify-content-center mb-0">
        <div class="card-body">
            <h2 class="mb-2 text-center">Cambio de contraseña</h2>
            <p class="text-center">Ingresa tu nueva contraseña</p>
            <form class="plp5 prp5">
                <input id="id_hidden" type="hidden" value="<?php echo $_GET['i'];?>">
                <div class="row">
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="password" class="form-label">Contraseña</label>
                            <input type="password" class="form-control" id="pass1_reg" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-6 mb20">
                        <div class="form-group">
                            <label for="confirm-password" class="form-label">Confirmar contraseña</label>
                            <input type="password" class="form-control" id="pass2_reg" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center">
                    <button id="btn-registrase" type="button" class="btn btn-warning" data-loading-text="Enviando..." onclick="guardar_pass()">Guardar</button>
                </div>
                
                <p class="mt-3 text-center">
                    Si ya tienes cuenta <a onclick="open_popup_login()" class="text-underline pointer">Ingresa aquí</a>
                </p>
            </form>
        </div>
    </div>
</div>

</div>    
<?php include 'footer.php';?>
</body>
</html>