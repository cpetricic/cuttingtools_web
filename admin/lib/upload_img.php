<?php
$targetFolder = '../../images/producto/';

$resultado = array();
if (!empty($_FILES)) {
    for($i=0;$i<count($_FILES);$i++){
        $nueva_imagen = md5(date("Y-m-d H:i:s").$_FILES[$i]['name']);
        $tempFile = $_FILES[$i]['tmp_name'];
        $targetPath = '../../images/producto/';

        $sep=explode('.',$_FILES[$i]['name']);
        $ext = '.'.$sep[count($sep)-1];
        $targetFile = rtrim($targetPath,'/') . '/' .$nueva_imagen.$ext;
        if(move_uploaded_file($tempFile,$targetFile)){
            $resultado[]=array(
                'nombre_original'=>$_FILES[$i]['name'],
                'nombre_modificado'=>$nueva_imagen.$ext,
                'estado'=>1,
            );
            
        }else{
            $resultado[]=array(
                'nombre_original'=>$_FILES[$i]['name'],
                'nombre_modificado'=>$nueva_imagen.$ext,
                'estado'=>0,
            );
        }
    }
}
echo json_encode($resultado);
?>