<?php   
include('conex.php');
extract($_REQUEST);

switch($idfuncion) {

    case 1://GET DATOS
		$datos = array();
		$query="SELECT subcategoria.id_subcategoria, 
                       subcategoria.id_categoria,
                       subcategoria.nombre_subcategoria,
                       categoria.nombre_categoria,
                       subcategoria.estado,
                       subcategoria.fecha_ingreso
				FROM subcategoria INNER JOIN categoria ON categoria.id_categoria=subcategoria.id_categoria
                ORDER BY subcategoria.id_subcategoria DESC";
		$result=mysql_query($query,$link);
		while($row=mysql_fetch_array($result)){
			$datos[] = array(
                'id'                => utf8_encode($row['id_subcategoria']),
                'nombre'            => utf8_encode($row['nombre_subcategoria']), 
                'id_categoria'      => utf8_encode($row['id_categoria']), 
                'nombre_categoria'  => utf8_encode($row['nombre_categoria']), 
                'estado'            => utf8_encode($row['estado']), 
                'fecha'             => date_format(date_create($row['fecha_ingreso']), 'd-m-Y H:i:s')
			);
		}

		echo json_encode($datos);
	break;
    case 2://GET DATOS X ID
		$query="SELECT subcategoria.id_subcategoria, 
                       subcategoria.id_categoria,
                       subcategoria.nombre_subcategoria,
                       categoria.nombre_categoria,
                       subcategoria.estado,
                       subcategoria.fecha_ingreso
				FROM subcategoria INNER JOIN categoria ON categoria.id_categoria=subcategoria.id_categoria 
                WHERE id_subcategoria=".$id;
		$result=mysql_query($query,$link);
		$row=mysql_fetch_array($result);
        $datos = array(
            'id'                => utf8_encode($row['id_subcategoria']),
            'nombre'            => utf8_encode($row['nombre_subcategoria']), 
            'id_categoria'      => utf8_encode($row['id_categoria']), 
            'nombre_categoria'  => utf8_encode($row['nombre_categoria']), 
            'estado'            => utf8_encode($row['estado']), 
            'fecha'             => date_format(date_create($row['fecha_ingreso']), 'd-m-Y H:i:s')
        );
		echo json_encode($datos);
	break;
    case 3://DELETE X ID
		$datos = array();
		$query="DELETE FROM subcategoria WHERE id_subcategoria=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 4://CAMBIO ESTADO
		$datos = array();
        if($estado==1){
            $est_new=0;
        }else{
            $est_new=1;
        }
		$query="UPDATE subcategoria SET estado=".$est_new." WHERE id_subcategoria=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 5://EDITAR
		$datos = array();
		$query="UPDATE subcategoria SET 
                    id_categoria='".trim(utf8_decode($id_categoria))."', 
                    nombre_subcategoria='".trim(utf8_decode($nombre))."' 
                WHERE id_subcategoria=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 6://INSERTAR
		$datos = array();
		$query="INSERT INTO subcategoria(id_categoria, nombre_subcategoria) VALUES ('".trim(utf8_decode($id_categoria))."', '".trim(utf8_decode($nombre))."')";
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;

    
}

?>
