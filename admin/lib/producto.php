<?php   
include('conex.php');
extract($_REQUEST);

switch($idfuncion) {

    case 1://GET DATOS CATEGORIAS ACTIVAS
		$datos = array();
		$query="SELECT id_categoria, nombre_categoria FROM categoria WHERE estado=1 ORDER BY nombre_categoria ASC";
		$result=mysql_query($query,$link);
		while($row=mysql_fetch_array($result)){
			$datos[] = array(
                'id'        => utf8_encode($row['id_categoria']),
                'nombre'    => utf8_encode($row['nombre_categoria'])
			);
		}
		echo json_encode($datos);
	break;
    case 2://GET DATOS SUBCATEGORIAS ACTIVAS X ID CATEGORIA
		$datos = array();
		$query="SELECT id_subcategoria, nombre_subcategoria FROM subcategoria WHERE estado=1 AND id_categoria=".$id_categoria." ORDER BY nombre_subcategoria ASC";
		$result=mysql_query($query,$link);
		while($row=mysql_fetch_array($result)){
			$datos[] = array(
                'id'        => utf8_encode($row['id_subcategoria']),
                'nombre'    => utf8_encode($row['nombre_subcategoria'])
			);
		}
		echo json_encode($datos);
	break;
    case 3://INSERTAR
		$datos = array();
		$query="INSERT INTO producto(id_categoria,
                                     id_subcategoria,
                                     sku_producto,
                                     nombre_producto,
                                     marca_producto,
                                     precio_venta_producto,
                                     precio_normal_producto,
                                     id_grupo,
                                     descripcion_producto,
                                     orden) 
                    VALUES ('".trim(utf8_decode($id_categoria))."', 
                            '".trim(utf8_decode($id_subcategoria))."', 
                            '".trim(utf8_decode($sku))."', 
                            '".trim(utf8_decode($nombre))."', 
                            '".trim(utf8_decode($marca))."', 
                            '".trim(utf8_decode($precio_venta))."', 
                            '".trim(utf8_decode($precio_normal))."', 
                            '".trim(utf8_decode($grupo))."', 
                            '".trim(utf8_decode($descripcion))."', 
                            '".trim(utf8_decode($posicion))."')";
		if(mysql_query($query,$link)){
            $datos['id'] = mysql_insert_id($link);
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 4://INSERTAR IMAGENES
        $datos = array();
        $datos['estado']=0;
        for($i=0;$i<count($imagenes);$i++){
            $query="INSERT INTO imagen_producto(id_producto, nombre_imagen_producto, estado) 
                        VALUES ('".trim(utf8_decode($id_producto))."', 
                                '".trim(utf8_decode($imagenes[$i]))."',
                                1)";
            if(mysql_query($query,$link)){
                $datos['estado']++;
            }else{
                $datos['estado']=0;
            }
        }
		echo json_encode($datos);
    break;
    case 5://GET DATOS
		$datos = array();
		$query="SELECT id_producto,
                       id_grupo,
                       id_categoria,
                       id_subcategoria,
                       sku_producto,
                       nombre_producto,
                       marca_producto,
                       precio_venta_producto,
                       precio_normal_producto,
                       descripcion_producto,
                       destacado,
                       orden,
                       estado,
                       fecha_ingreso
				FROM producto
                ORDER BY id_producto ASC";
		$result=mysql_query($query,$link);
		while($row=mysql_fetch_array($result)){
            $query2="SELECT id_categoria, nombre_categoria FROM categoria WHERE id_categoria=".$row['id_categoria'];
            $result2=mysql_query($query2,$link);
            $row2=mysql_fetch_array($result2);

            if($row['id_subcategoria']>0){
                $query3="SELECT id_subcategoria, nombre_subcategoria FROM subcategoria WHERE id_subcategoria=".$row['id_subcategoria'];
                $result3=mysql_query($query3,$link);
                $row3=mysql_fetch_array($result3);
                $id_subcategoria = utf8_encode($row3['id_subcategoria']);
                $nombre_categoria = utf8_encode($row3['nombre_subcategoria']);
            }else{
                $id_subcategoria = 0;
                $nombre_categoria = '';
            }

			$datos[] = array(
                'id_producto'           => utf8_encode($row['id_producto']),
                'id_grupo'              => utf8_encode($row['id_grupo']), 
                'id_categoria'          => utf8_encode($row2['id_categoria']), 
                'nombre_categoria'      => utf8_encode($row2['nombre_categoria']), 
                'id_subcategoria'       => $id_subcategoria, 
                'nombre_subcategoria'   => $nombre_categoria, 
                'sku'                   => utf8_encode($row['sku_producto']),
                'nombre'                => utf8_encode($row['nombre_producto']),
                'marca'                 => utf8_encode($row['marca_producto']),
                'precio_venta'          => number_format($row['precio_venta_producto'],0,',','.'),
                'precio_normal'         => number_format($row['precio_normal_producto'],0,',','.'),
                'descripcion'           => utf8_encode($row['descripcion_producto']),
                'destacado'             => utf8_encode($row['destacado']),
                'orden'                 => utf8_encode($row['orden']),
                'estado'                => utf8_encode($row['estado']), 
                'fecha'                 => date_format(date_create($row['fecha_ingreso']), 'd-m-Y H:i:s')
			);
		}

		echo json_encode($datos);
	break;
    case 6://CAMBIO ESTADO
		$datos = array();
        if($estado==1){
            $est_new=0;
        }else{
            $est_new=1;
        }
		$query="UPDATE producto SET estado=".$est_new." WHERE id_producto=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 7://DELETE X ID
		$datos = array();
		$query="DELETE FROM producto WHERE id_producto=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 8://GET DATOS
		$datos = array();
		$query="SELECT id_producto,
                       id_grupo,
                       id_categoria,
                       id_subcategoria,
                       sku_producto,
                       nombre_producto,
                       marca_producto,
                       precio_venta_producto,
                       precio_normal_producto,
                       descripcion_producto,
                       orden,
                       estado,
                       fecha_ingreso
				FROM producto WHERE id_producto=".$id."
                ORDER BY id_producto ASC";
		$result=mysql_query($query,$link);
		$row=mysql_fetch_array($result);

        $images = array();
        $query2="SELECT id_imagen_producto, nombre_imagen_producto FROM imagen_producto WHERE id_producto=".$row['id_producto'];
        $result2=mysql_query($query2,$link);
        while($row2=mysql_fetch_array($result2)){
            $images[] = array(
                'id'    =>utf8_encode($row2['id_imagen_producto']),
                'imagen'=>utf8_encode($row2['nombre_imagen_producto'])
            );
        }


        $datos = array(
            'images'                => $images,
            'id_producto'           => utf8_encode($row['id_producto']),
            'id_grupo'              => utf8_encode($row['id_grupo']),
            'id_categoria'          => utf8_encode($row['id_categoria']), 
            'id_subcategoria'       => utf8_encode($row['id_subcategoria']), 
            'sku'                   => utf8_encode($row['sku_producto']),
            'nombre'                => utf8_encode($row['nombre_producto']),
            'marca'                 => utf8_encode($row['marca_producto']),
            'precio_venta'          => $row['precio_venta_producto'],
            'precio_normal'         => $row['precio_normal_producto'],
            'descripcion'           => utf8_encode($row['descripcion_producto']),
            'orden'                 => utf8_encode($row['orden']),
            'estado'                => utf8_encode($row['estado']), 
            'fecha'                 => date_format(date_create($row['fecha_ingreso']), 'd-m-Y H:i:s')
        );
		

		echo json_encode($datos);
	break;
    case 9://DELETE IMAGEN X ID
		$datos = array();
		$query="DELETE FROM imagen_producto WHERE id_imagen_producto=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 10://EDITAR
		$query="UPDATE producto SET 
                    id_grupo='".utf8_decode($grupo)."', 
                    id_categoria='".utf8_decode($id_categoria)."', 
                    id_subcategoria='".utf8_decode($id_subcategoria)."', 
                    sku_producto='".utf8_decode($sku)."', 
                    nombre_producto='".utf8_decode($nombre)."', 
                    marca_producto='".utf8_decode($marca)."', 
                    precio_venta_producto='".utf8_decode($precio_venta)."', 
                    precio_normal_producto='".utf8_decode($precio_normal)."', 
                    descripcion_producto='".utf8_decode($descripcion)."', 
                    orden='".utf8_decode($posicion)."',
                    fecha_modificacion='".date("Y-m-d H:i:s")."'
                WHERE id_producto=".$id_producto;
               
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
        
		echo json_encode($datos);
	break;
    case 11://CAMBIO DESTACADO
		$datos = array();
        if($estado==1){
            $est_new=0;
        }else{
            $est_new=1;
        }
		$query="UPDATE producto SET destacado=".$est_new." WHERE id_producto=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 12:
        $datos = array();
		$query="SELECT id_producto, MAX(id_grupo)
                    FROM producto
                    ORDER BY MAX(id_grupo) DESC";
		$result=mysql_query($query,$link);
		$row=mysql_fetch_array($result);
        $grupo = (int)$row[1] + 1;

        $filtro='';
        for($i=0;$i<count($ids);$i++){
            if($i==0){
                $filtro.=' id_producto='.$ids[$i];
            }else{
                $filtro.=' OR id_producto='.$ids[$i];
            }
        }

        $query2="UPDATE producto SET id_grupo=".$grupo." WHERE ".$filtro;
        if(mysql_query($query2,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }

		echo json_encode($datos);
    break;
    case 13:
        $datos = array();

        $filtro='';
        for($i=0;$i<count($ids);$i++){
            if($i==0){
                $filtro.=' id_producto='.$ids[$i];
            }else{
                $filtro.=' OR id_producto='.$ids[$i];
            }
        }

        $query2="UPDATE producto SET id_grupo=0 WHERE ".$filtro;
        if(mysql_query($query2,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }

		echo json_encode($datos);
    break;
}

?>
