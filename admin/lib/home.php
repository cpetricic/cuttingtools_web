<?php   
include('conex.php');
extract($_REQUEST);

switch($idfuncion) {
    case 1://GET DATOS - slider_home
		$datos = array();
		$query="SELECT 	id_slider_home, image_slider_home, url_slider_home, orden, estado, fecha_ingreso
                    FROM slider_home
                    ORDER BY id_slider_home DESC";
		$result=mysql_query($query,$link);
		while($row=mysql_fetch_array($result)){
			$datos[] = array(
                'id'       => utf8_encode($row['id_slider_home']),
                'imagen'   => utf8_encode($row['image_slider_home']), 
                'url'      => utf8_encode($row['url_slider_home']), 
                'orden'    => utf8_encode($row['orden']),
                'estado'   => utf8_encode($row['estado']), 
                'fecha'    => date_format(date_create($row['fecha_ingreso']), 'd-m-Y H:i:s')
			);
		}
		echo json_encode($datos);
	break;
    case 2://INSERTAR - slider_home
		$datos = array();

        if (!empty($_FILES)) {
            $nueva_imagen = md5(date("Y-m-d H:i:s").$_FILES['file']['name']);
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath = '../../images/home/slider/';

            $sep=explode('.',$_FILES['file']['name']);
            $ext = '.'.$sep[count($sep)-1];
            $targetFile = rtrim($targetPath,'/') . '/' .$nueva_imagen.$ext;
            if(move_uploaded_file($tempFile,$targetFile)){
                $resultado=array(
                    'nombre_original'=>$_FILES['file']['name'],
                    'nombre_modificado'=>$nueva_imagen.$ext,
                    'estado'=>1,
                );
                
            }else{
                $resultado=array(
                    'nombre_original'=>$_FILES['file']['name'],
                    'nombre_modificado'=>$nueva_imagen.$ext,
                    'estado'=>0,
                );
            }
        }
        if($resultado['estado']==1){
            $query="INSERT INTO slider_home(image_slider_home, url_slider_home, orden) 
                        VALUES ('".trim(utf8_decode($resultado['nombre_modificado']))."', 
                                '".trim(utf8_decode($url))."', 
                                '".trim(utf8_decode($orden))."')";
            if(mysql_query($query,$link)){
                $datos['estado']=1;
            }else{
                $datos['estado']=0;
            }
        }else{
            $datos['estado'] = 0;
        }

		echo json_encode($datos);
	break;
    case 3://CAMBIO ESTADO - slider_home
		$datos = array();
        if($estado==1){
            $est_new=0;
        }else{
            $est_new=1;
        }
		$query="UPDATE slider_home SET estado=".$est_new." WHERE id_slider_home=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 4://DELETE X ID - slider_home
		$datos = array();
		$query="DELETE FROM slider_home WHERE id_slider_home=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 5://UPDATE - slider_home
		$datos = array();

        switch($campo){
            case 1:
                $update="url_slider_home='".utf8_decode($url)."'";
            break;
            case 2:
                $update="orden=".$orden;
            break;
        }

		$query="UPDATE slider_home SET ".$update." WHERE id_slider_home=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;

    case 6://GET DATOS - banner_home
		$datos = array();
		$query="SELECT id_banner_home, imagen_banner_home, url_banner_home, posicion, estado, fecha_ingreso 
                    FROM banner_home
                    ORDER BY id_banner_home DESC";
		$result=mysql_query($query,$link);
		while($row=mysql_fetch_array($result)){
			$datos[] = array(
                'id'       => utf8_encode($row['id_banner_home']),
                'imagen'   => utf8_encode($row['imagen_banner_home']), 
                'url'      => utf8_encode($row['url_banner_home']), 
                'posicion' => utf8_encode($row['posicion']),
                'estado'   => utf8_encode($row['estado']), 
                'fecha'    => date_format(date_create($row['fecha_ingreso']), 'd-m-Y H:i:s')
			);
		}
		echo json_encode($datos);
	break;
    case 7://INSERTAR - banner_home
		$datos = array();

        if (!empty($_FILES)) {
            $nueva_imagen = md5(date("Y-m-d H:i:s").$_FILES['file']['name']);
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath = '../../images/home/banner/';

            $sep=explode('.',$_FILES['file']['name']);
            $ext = '.'.$sep[count($sep)-1];
            $targetFile = rtrim($targetPath,'/') . '/' .$nueva_imagen.$ext;
            if(move_uploaded_file($tempFile,$targetFile)){
                $resultado=array(
                    'nombre_original'=>$_FILES['file']['name'],
                    'nombre_modificado'=>$nueva_imagen.$ext,
                    'estado'=>1,
                );
                
            }else{
                $resultado=array(
                    'nombre_original'=>$_FILES['file']['name'],
                    'nombre_modificado'=>$nueva_imagen.$ext,
                    'estado'=>0,
                );
            }
        }
        if($resultado['estado']==1){
            $query="INSERT INTO banner_home(imagen_banner_home, url_banner_home, posicion) 
                        VALUES ('".trim(utf8_decode($resultado['nombre_modificado']))."', 
                                '".trim(utf8_decode($url))."', 
                                '".trim(utf8_decode($posicion))."')";
            if(mysql_query($query,$link)){
                $datos['estado']=1;
            }else{
                $datos['estado']=0;
            }
        }else{
            $datos['estado'] = 0;
        }

		echo json_encode($datos);
	break;
    case 8://CAMBIO ESTADO - banner_home
		$datos = array();
        if($estado==1){
            $est_new=0;
        }else{
            $est_new=1;
        }
        if($est_new==1){
            $query2="UPDATE banner_home SET estado=0 WHERE posicion=".$posicion;
		    mysql_query($query2,$link);
        }

		$query="UPDATE banner_home SET estado=".$est_new." WHERE id_banner_home=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 9://DELETE X ID - banner_home
		$datos = array();
		$query="DELETE FROM banner_home WHERE id_banner_home=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
    case 10://UPDATE - banner_home
		$datos = array();
		$query="UPDATE banner_home SET url_banner_home='".utf8_decode($url)."' WHERE id_banner_home=".$id;
		if(mysql_query($query,$link)){
            $datos['estado']=1;
        }else{
            $datos['estado']=0;
        }
		echo json_encode($datos);
	break;
}

?>
