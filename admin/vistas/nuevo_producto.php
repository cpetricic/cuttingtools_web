<?php include 'header.php';?>
<?php include 'menu.php';?>
<script>
var url_metodo='../lib/producto.php';     
var editor
$(document).ready(function(){
    editor = new RichTextEditor("#descripcion");

    get_categorias_activas();
    $('.select_categoria').change(function(){
        var id_categoria = $('.select_categoria').val();
        if(id_categoria==''){
            $('.select_subcategoria').html('');
        }else{
            get_subcategorias_activas(id_categoria);
        }
    });
});
function get_categorias_activas(){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:1},
		dataType: 'json',
		success: function(a){
            var html='<option value="">Seleccione una categoría</option>';
			for(var i=0;i<a.length;i++){
                html+='<option value="'+a[i].id+'">'+a[i].nombre+'</option>';
            }
            $('.select_categoria').html(html);
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
} 
function get_subcategorias_activas(id_categoria){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:2, id_categoria:id_categoria},
		dataType: 'json',
		success: function(a){
            var html='<option value="">Seleccione una subcategoría</option>';
			for(var i=0;i<a.length;i++){
                html+='<option value="'+a[i].id+'">'+a[i].nombre+'</option>';
            }
            $('.select_subcategoria').html(html);
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function ingresar(){
    var nombre = $('#nombre').val();
    var marca = $('#marca').val();
    var sku = $('#sku').val();
    var precio_venta = $('#precio_venta').val();
    var precio_normal = $('#precio_normal').val();
    var grupo = $('#grupo').val();
    var posicion = $('#posicion').val();
    var id_categoria = $('#categoria').val();
    var id_subcategoria = $('#subcategoria').val();
    var descripcion = editor.getHTMLCode();

    if(nombre==''){
        error('Debe ingresar el nombre');
        $('#nombre').focus();
        return;
    }
    if(marca==''){
        error('Debe ingresar la marca');
        $('#marca').focus();
        return;
    }
    if(sku==''){
        error('Debe ingresar el SKU');
        $('#sku').focus();
        return;
    }
    if(precio_venta==''){
        error('Debe ingresar el precio de venta');
        $('#precio_venta').focus();
        return;
    }
    if(precio_normal==''){
        error('Debe ingresar el precio normal');
        $('#precio_normal').focus();
        return;
    }
    if(posicion==''){
        error('Debe ingresar la posición');
        $('#posicion').focus();
        return;
    }

    if(id_categoria==''){
        error('Debe seleccionar una categoria');
        $('#categoria').focus();
        return;
    }
    if(grupo==''){
        grupo=0;
    }
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:3, grupo:grupo, nombre:nombre, marca:marca, sku:sku, precio_venta:precio_venta, precio_normal:precio_normal, posicion:posicion, id_categoria:id_categoria, id_subcategoria:id_subcategoria, descripcion:descripcion},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
                    if($('#images').prop('files').length>0){
                        upload_img(a.id);
                    }else{
                        ok('Ingresado correctamente');
                        top.location.href="ver_productos";
                    }
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
} 
function upload_img(id_producto){
    var images = $('#images').prop('files');
    var form_data = new FormData();
    for(var i=0;i<images.length;i++){
        form_data.append(i, $('#images').prop('files')[i]);
    }

    $.ajax({
        url: '../lib/upload_img.php',
        type: 'POST',
        data: form_data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            data = JSON.parse(data);
            //console.log(data);
            var imagenes_ok=[];
            for(i=0;i<data.length;i++){
                if(data[i].estado==1){
                    imagenes_ok.push(data[i].nombre_modificado);
                }
            }
            insert_image(id_producto, imagenes_ok);
        },
    });
}
function insert_image(id_producto, imagenes_ok){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:4, id_producto:id_producto, imagenes:imagenes_ok},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				default:
					ok('Ingresado correctamente');
                    top.location.href="ver_productos";
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
</script>
<div class="content-inner container-fluid pb-0">
    <div class="card">
         <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Nuevo producto</h4>
            </div>
            <button type="button" class="btn btn-success" onclick="ingresar()">Guardar</button>
         </div>
         <div class="card-body">
            <form>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Nombre Producto</label>
                            <input type="text" class="form-control" id="nombre" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Marca</label>
                            <input type="text" class="form-control" id="marca" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">SKU</label>
                            <input type="text" class="form-control" id="sku" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Precio de venta</label>
                            <input type="number" class="form-control" id="precio_venta" value="0" min="0" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Precio normal</label>
                            <input type="number" class="form-control" id="precio_normal" value="0" min="0" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Posición</label>
                            <input type="number" class="form-control" id="posicion" value="1" min="1" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Categoría</label>
                            <select id="categoria" class="form-select mb-3 shadow-none select_categoria"></select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Subategoría</label>
                            <select id="subcategoria" class="form-select mb-3 shadow-none select_subcategoria"></select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Grupo (valor 0 sin grupo)</label>
                            <input type="number" class="form-control" id="grupo" value="0" min="0" placeholder="">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-label">Especificaciones</label>
                            <div id="descripcion" style="max-width:100%"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Imagenes producto</h4>
            </div>
        </div>
        <div class="card-body">
            <form enctype="multipart/form-data">
                <input class="form-control" type="file" name="file" id="images" accept="image/*" multiple>
            </form>
        </div>
    </div>
</div> 



<?php include 'footer.php';?>   

