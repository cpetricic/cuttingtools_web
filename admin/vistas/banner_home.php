<?php include 'header.php';?>
<?php include 'menu.php';?>
<script>
var url_metodo='../lib/home.php';     
$(document).ready(function(){
    get_datos();
});
function get_datos(){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:6},
		dataType: 'json',
		success: function(a){
            var array_posicion = ['','Fila 1 - Banner izquierdo','Fila 1 - Banner derecho','Fila 2 - Banner 1','Fila 2 - Banner 2','Fila 2 - Banner 3','Fila 2 - Banner 4','Nuevas promociones - Banner izquierdo','Nuevas promociones - Banner derecho 1','Nuevas promociones - Banner derecho 2'];
            var html='<table id="datatable" class="table" data-toggle="data-table">'+
                        '<thead>'+
                            '<tr>'+
                                '<th>ID</th>'+
                                '<th>Imagen</th>'+
                                '<th>URL</th>'+
                                '<th>Posición</th>'+
                                '<th>Fecha ingreso</th>'+
                                '<th>Estado</th>'+
                                '<th>Eliminar</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
                        
			for(var i=0;i<a.length;i++){
                if(a[i].estado==1){
                    var status = '<span class="badge bg-soft-success p-2 text-success pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id+','+a[i].posicion+')">Activo</span>';
                }else{
                    var status = '<span class="badge bg-soft-danger p-2 text-danger pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id+','+a[i].posicion+')">Inactivo</span>';
                }
                html+='<tr>'+
                        '<td>'+a[i].id+'</td>'+
                        '<td><a data-fancybox="gallery'+a[i].id+'" href="../../images/home/banner/'+a[i].imagen+'"><img src="../../images/home/banner/'+a[i].imagen+'" class="d-block" height="50" alt=""></a></td>'+
                        '<td><input id="camp_url_'+a[i].id+'" type="text" class="form-control" onchange="update_campo('+a[i].id+')" name="url" value="'+a[i].url+'"></td>'+
                        '<td>'+array_posicion[a[i].posicion]+'</td>'+
                        '<td>'+a[i].fecha+'</td>'+
                        '<td>'+status+'</td>'+
                        '<td>'+
                            '<button type="button" class="btn btn-sm btn-danger" onclick="open_confirm('+a[i].id+')">Eliminar</button>'+
                        '</td>'+
                     '</tr>';
            }
            html+='</tbody></table>';
            $('#body_table').html(html);

            $('[data-toggle="data-table"]').DataTable({
                "language": {
                    "url": '../assets/vendor/language/es-ES.json'
                }
            });
            Fancybox.bind("[data-fancybox]", {
                // Your custom options
            });
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function ingresar(){
    var images = $('#images').prop('files');
    var url = $('#url').val();
    var posicion = $('#posicion').val();

    if(images.length==0){
        error('Debe seleccionar una imagen');
        return;
    }

    var form_data = new FormData();
    form_data.append('file', images[0]);
    form_data.append('idfuncion', 7);
    form_data.append('url', url);
    form_data.append('posicion', posicion);

    $.ajax({
        url: url_metodo,
        type: 'POST',
        data: form_data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (a) {
            a = JSON.parse(a);
            console.log(a);
            switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				default:
					ok('Ingresado correctamente');
                    get_datos();
                    $('#images').val('');
                    $('#url').val('');
                    $('#posicion').val(1);
				break;
			}
        },
    });
}
function cambio_estado(estado, id, posicion){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:8, id:id, estado:estado, posicion:posicion},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Cambiado correctamente');
                    get_datos();
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function eliminar(id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:9, id:id},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Eliminado correctamente');
                    get_datos();
                    $('#confirmModal').modal('hide');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function open_confirm(id){
    $('#btn_confirm_si').attr('onclick', 'eliminar('+id+')');
    $('#confirmModal').modal('show');
} 

function update_campo(id){
    var url = $('#camp_url_'+id).val();
    
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:10, id:id, url:url},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Cambiado correctamente');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
    
}

</script>
<div class="content-inner container-fluid pb-0">
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title mt-2">Nuevo banners home</h4>
                    </div>
                    <button type="button" class="btn btn-success" onclick="ingresar()">Guardar</button>
                </div>
                <div class="card-body">
                    <form enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Imagen</label>
                                    <input class="form-control" type="file" name="file" id="images" accept="image/*">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">URL</label>
                                    <input type="text" class="form-control" name="url" id="url" placeholder="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Posición</label>
                                    <select class="form-select" data-trigger="" name="choices-single-default" id="posicion">
                                        <optgroup label="Fila 1">
                                            <option value="1">Banner izquierdo</option>
                                            <option value="2">Banner derecho</option>
                                        </optgroup>
                                        <optgroup label="Fila 2">
                                            <option value="3">Banner 1</option>
                                            <option value="4">Banner 2</option>
                                            <option value="5">Banner 3</option>
                                            <option value="6">Banner 4</option>
                                        </optgroup>
                                        <optgroup label="Nuevas promociones">
                                            <option value="7">Banner izquierdo</option>
                                            <option value="8">Banner derecho 1</option>
                                            <option value="9">Banner derecho 2</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title mt-2">Posiciones banner home</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mx-auto">
                        <div class="col-6 px2">
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner izquierdo</p>
                                        <p class="h6 mb-0 text-white ">920x490</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px2">
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner derecho</p>
                                        <p class="h6 mb-0 text-white ">920x490</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-3 px2">
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner 1</p>
                                        <p class="h6 mb-0 text-white ">455x470</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 px2">
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner 2</p>
                                        <p class="h6 mb-0 text-white ">455x470</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 px2">
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner 3</p>
                                        <p class="h6 mb-0 text-white ">455x470</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 px2">
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner 4</p>
                                        <p class="h6 mb-0 text-white ">455x470</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-6 px2">
                            <div class="card mb-1 text-white bg-secondary" style="height: 98%;">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner izquierdo</p>
                                        <p class="h6 mb-0 text-white ">920x740</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px2">
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner derecho 1</p>
                                        <p class="h6 mb-0 text-white ">920x460</p>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="card mb-1 text-white bg-secondary">
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p class="font-size-14 mb-0">Banner derecho 2</p>
                                        <p class="h6 mb-0 text-white ">920x280</p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="card">
         <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Banners home</h4>
            </div>
         </div>
         <div class="card-body">
            <div id="body_table" class="table-responsive border rounded"></div>
        </div>
    </div>

</div> 



<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="confirmModalLabel">Eliminar</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button id="btn_confirm_si" type="button" class="btn btn-success">Si</button>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php';?>   

