<?php include 'header.php';?>
<?php include 'menu.php';?>
<script>
var url_metodo='../lib/home.php';     
$(document).ready(function(){
    get_datos();
});
function get_datos(){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:1},
		dataType: 'json',
		success: function(a){
            var html='<table id="datatable" class="table" data-toggle="data-table">'+
                        '<thead>'+
                            '<tr>'+
                                '<th>ID</th>'+
                                '<th>Imagen</th>'+
                                '<th>URL</th>'+
                                '<th>Orden</th>'+
                                '<th>Fecha ingreso</th>'+
                                '<th>Estado</th>'+
                                '<th>Eliminar</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
                        
			for(var i=0;i<a.length;i++){
                if(a[i].estado==1){
                    var status = '<span class="badge bg-soft-success p-2 text-success pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id+')">Activo</span>';
                }else{
                    var status = '<span class="badge bg-soft-danger p-2 text-danger pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id+')">Inactivo</span>';
                }
                html+='<tr>'+
                        '<td>'+a[i].id+'</td>'+
                        '<td><a data-fancybox="gallery'+a[i].id+'" href="../../images/home/slider/'+a[i].imagen+'"><img src="../../images/home/slider/'+a[i].imagen+'" class="d-block" height="50" alt=""></a></td>'+
                        '<td><input id="camp_url_'+a[i].id+'" type="text" class="form-control" onchange="update_campo(1, '+a[i].id+')" name="url" value="'+a[i].url+'"></td>'+
                        '<td><input id="camp_orden_'+a[i].id+'" type="number" class="form-control" onchange="update_campo(2, '+a[i].id+')" name="orden" min="1" value="'+a[i].orden+'"></td>'+
                        '<td>'+a[i].fecha+'</td>'+
                        '<td>'+status+'</td>'+
                        '<td>'+
                            '<button type="button" class="btn btn-sm btn-danger" onclick="open_confirm('+a[i].id+')">Eliminar</button>'+
                        '</td>'+
                     '</tr>';
            }
            html+='</tbody></table>';
            $('#body_table').html(html);

            $('[data-toggle="data-table"]').DataTable({
                "language": {
                    "url": '../assets/vendor/language/es-ES.json'
                }
            });
            Fancybox.bind("[data-fancybox]", {
                // Your custom options
            });
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function cambio_estado(estado, id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:3, id:id, estado:estado},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Cambiado correctamente');
                    get_datos();
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function eliminar(id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:4, id:id},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Eliminado correctamente');
                    get_datos();
                    $('#confirmModal').modal('hide');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function open_confirm(id){
    $('#btn_confirm_si').attr('onclick', 'eliminar('+id+')');
    $('#confirmModal').modal('show');
} 
function ingresar(){
    var images = $('#images').prop('files');
    var url = $('#url').val();
    var orden = $('#orden').val();

    if(images.length==0){
        error('Debe seleccionar una imagen');
        return;
    }
    if(orden==""){
        error('Debe ingresar el orden');
        $('#orden').focus();
        return;
    }

    var form_data = new FormData();
    form_data.append('file', images[0]);
    form_data.append('idfuncion', 2);
    form_data.append('url', url);
    form_data.append('orden', orden);

    $.ajax({
        url: url_metodo,
        type: 'POST',
        data: form_data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (a) {
            a = JSON.parse(a);
            console.log(a);
            switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				default:
					ok('Ingresado correctamente');
                    get_datos();
                    $('#images').val('');
                    $('#url').val('');
                    $('#orden').val(1);
				break;
			}
        },
    });
}
function update_campo(campo, id){
    var url = $('#camp_url_'+id).val();
    var orden = $('#camp_orden_'+id).val();
    if(campo==2){
        if(orden==""){
            error('Debe ingresar el orden');
            $('#camp_orden_'+id).focus();
            return;
        }
    }
    
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:5, id:id, campo:campo, url:url, orden:orden},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Cambiado correctamente');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
    
}

</script>
<div class="content-inner container-fluid pb-0">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Nuevo slider home</h4>
            </div>
            <button type="button" class="btn btn-success" onclick="ingresar()">Guardar</button>
        </div>
        <div class="card-body">
            <form enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Imagen</label>
                            <input class="form-control" type="file" name="file" id="images" accept="image/*">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">URL</label>
                            <input type="text" class="form-control" name="url" id="url" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Orden</label>
                            <input type="number" class="form-control" name="orden" id="orden" min="1" value="1" placeholder="">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
         <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Slider home</h4>
            </div>
         </div>
         <div class="card-body">
            <div id="body_table" class="table-responsive border rounded"></div>
        </div>
    </div>

</div> 



<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="confirmModalLabel">Eliminar</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button id="btn_confirm_si" type="button" class="btn btn-success">Si</button>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php';?>   

