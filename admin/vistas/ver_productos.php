<?php include 'header.php';?>
<?php include 'menu.php';?>
<script>
var url_metodo='../lib/producto.php';     
$(document).ready(function(){
    get_datos();
});
function get_datos(){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:5},
		dataType: 'json',
		success: function(a){
            var html='<table id="datatable" class="table" data-toggle="data-table">'+
                        '<thead>'+
                            '<tr>'+
                                '<th></th>'+
                                '<th>ID</th>'+
                                '<th>Producto</th>'+
                                '<th>Categoría</th>'+
                                '<th>Subcategoría</th>'+
                                '<th>SKU</th>'+
                                '<th>Marca</th>'+
                                '<th>Precio venta</th>'+
                                '<th>Precio normal</th>'+
                                '<th>Grupo</th>'+
                                '<th>Fecha ingreso</th>'+
                                '<th>Destacado</th>'+
                                '<th>Estado</th>'+
                                '<th>Acciones</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
                        
			for(var i=0;i<a.length;i++){
                if(a[i].estado==1){
                    var status = '<span class="badge bg-soft-success p-2 text-success pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id_producto+')">Activo</span>';
                }else{
                    var status = '<span class="badge bg-soft-danger p-2 text-danger pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id_producto+')">Inactivo</span>';
                }
                if(a[i].destacado==1){
                    var dest = '<span class="badge bg-soft-success p-2 text-success pointer" onclick="cambio_destacado('+a[i].destacado+','+a[i].id_producto+')">Si</span>';
                }else{
                    var dest = '<span class="badge bg-soft-danger p-2 text-danger pointer" onclick="cambio_destacado('+a[i].destacado+','+a[i].id_producto+')">No</span>';
                }
                if(a[i].id_grupo==0){
                    var html_grupo = 'Sin grupo';
                }else{
                    var html_grupo = a[i].id_grupo;
                }
                html+='<tr>'+
                        '<td><div class="form-check d-block"><input class="form-check-input checkprod" type="checkbox" value="'+a[i].id_producto+'" id="checkprod_'+a[i].id_producto+'"></div></td>'+
                        '<td>'+a[i].id_producto+'</td>'+
                        '<td>'+a[i].nombre+'</td>'+
                        '<td>'+a[i].nombre_categoria+'</td>'+
                        '<td>'+a[i].nombre_subcategoria+'</td>'+
                        '<td>'+a[i].sku+'</td>'+
                        '<td>'+a[i].marca+'</td>'+
                        '<td>$'+a[i].precio_venta+'</td>'+
                        '<td>$'+a[i].precio_normal+'</td>'+
                        '<td>'+html_grupo+'</td>'+
                        '<td>'+a[i].fecha+'</td>'+
                        '<td><center>'+dest+'</center></td>'+
                        '<td>'+status+'</td>'+
                        '<td>'+
                            '<button type="button" class="btn btn-sm btn-light" onclick="open_editar('+a[i].id_producto+')">Editar</button>'+
                            '<button type="button" class="btn btn-sm btn-danger" onclick="open_confirm('+a[i].id_producto+')">Eliminar</button>'+
                        '</td>'+
                     '</tr>';
            }
            html+='</tbody></table>';
            $('#body_table').html(html);

            $('[data-toggle="data-table"]').DataTable({
                "language": {
                    "url": '../assets/vendor/language/es-ES.json'
                }
            });
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function cambio_estado(estado, id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:6, id:id, estado:estado},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Cambiado correctamente');
                    get_datos();
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function cambio_destacado(estado, id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:11, id:id, estado:estado},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Cambiado correctamente');
                    get_datos();
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function eliminar(id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:7, id:id},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Eliminado correctamente');
                    get_datos();
                    $('#confirmModal').modal('hide');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function open_confirm(id){
    $('#btn_confirm_si').attr('onclick', 'eliminar('+id+')');
    $('#confirmModal').modal('show');
} 
function open_editar(id_producto){
    top.location.href="editar_producto?i="+id_producto;
} 
function crear_grupo(){
    var testval = [];
    $('.checkprod:checked').each(function() {
        testval.push($(this).val());
    });
    if(testval.length==0){
        error('Debe seleccionar un producto');
        return;
    }
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:12, ids:testval},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Grupo creado');
                    get_datos();
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
} 
function borrar_grupo(){
    var testval = [];
    $('.checkprod:checked').each(function() {
        testval.push($(this).val());
    });
    if(testval.length==0){
        error('Debe seleccionar un producto');
        return;
    }
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:13, ids:testval},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Grupo quitado');
                    get_datos();
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
} 
</script>
<div class="content-inner container-fluid pb-0">
    <div class="card">
         <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Productos</h4>
            </div>
            <a class="btn btn-success" href="nuevo_producto">Nuevo producto</a>
         </div>
         <div class="card-body">
            <div id="body_table" class="table-responsive border rounded mb-3"></div>
                            
            <div class="btn-group btn-group-toggle btn-group-sm btn-group-edges">
                <a class="button btn button-icon btn-success" onclick="crear_grupo()">Crear grupo con seleccionados</a>
                <a class="button btn button-icon btn-danger" onclick="borrar_grupo()">Quitar grupo a seleccionados</a>
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="confirmModalLabel">Eliminar</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button id="btn_confirm_si" type="button" class="btn btn-success">Si</button>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php';?>   