</main>
         <!-- Wrapper End-->

         <!-- Live Customizer end -->

         
       
<!-- Library Bundle Script -->
<script src="../assets/js/core/libs.min.js"></script>
<!-- Slider-tab Script -->
<script src="../assets/js/plugins/slider-tabs.js"></script>


<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css"/>

<!-- Lodash Utility -->
<script src="../assets/vendor/lodash/lodash.min.js"></script>
<!-- Utilities Functions -->
<script src="../assets/js/iqonic-script/utility.min.js"></script>
<!-- Settings Script -->
<script src="../assets/js/iqonic-script/setting.min.js"></script>
<!-- Settings Init Script -->
<script src="../assets/js/setting-init.js"></script>
<!-- External Library Bundle Script -->
<script src="../assets/js/core/external.min.js"></script>
<!-- Widgetchart Script -->
<script src="../assets/js/charts/widgetcharts.js?v=1.0.1" defer></script>
<!-- Dashboard Script -->
<script src="../assets/js/charts/dashboard.js?v=1.0.1" defer></script>
<!-- qompacui Script -->
<script src="../assets/js/qompac-ui.js?v=1.0.1" defer></script>
<script src="../assets/js/sidebar.js?v=1.0.1" defer></script>
</body>
</html>