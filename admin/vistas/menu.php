
   <!-- loader END -->
   <aside class="sidebar sidebar-base sidebar-white sidebar-default navs-rounded-all " id="first-tour" data-toggle="main-sidebar" data-sidebar="responsive">
      <div class="sidebar-header d-flex align-items-center justify-content-start">
            <a href="home" class="navbar-brand">
               <!--Logo start-->
               <div class="logo-main">
                  <div class="logo-normal">
                        <img src="../../images/logo.svg" class="img-fluid" alt="">
                  </div>
                  <div class="logo-mini">
                        <img src="../../images/logo.svg" class="img-fluid" alt="">
                  </div>
               </div>
            </a>
            <div class="sidebar-toggle" data-toggle="sidebar" data-active="true">
               <i class="icon">
                  <svg class="icon-10" width="10" height="10" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.29853 8C7.11974 8 6.94002 7.93083 6.80335 7.79248L3.53927 4.50446C3.40728 4.37085 3.33333 4.18987 3.33333 4.00036C3.33333 3.81179 3.40728 3.63081 3.53927 3.4972L6.80335 0.207279C7.07762 -0.069408 7.52132 -0.069408 7.79558 0.209174C8.06892 0.487756 8.06798 0.937847 7.79371 1.21453L5.02949 4.00036L7.79371 6.78618C8.06798 7.06286 8.06892 7.51201 7.79558 7.79059C7.65892 7.93083 7.47826 8 7.29853 8Z" fill="white"/>
                        <path d="M3.96552 8C3.78673 8 3.60701 7.93083 3.47034 7.79248L0.206261 4.50446C0.0742745 4.37085 0.000325203 4.18987 0.000325203 4.00036C0.000325203 3.81179 0.0742745 3.63081 0.206261 3.4972L3.47034 0.207279C3.74461 -0.069408 4.18831 -0.069408 4.46258 0.209174C4.73591 0.487756 4.73497 0.937847 4.4607 1.21453L1.69649 4.00036L4.4607 6.78618C4.73497 7.06286 4.73591 7.51201 4.46258 7.79059C4.32591 7.93083 4.14525 8 3.96552 8Z" fill="white"/>
                  </svg>
               </i>
            </div>
      </div>
      <div class="sidebar-body pt-0 data-scrollbar">
         <div class="sidebar-list">
            <!-- Sidebar Menu Start -->
            <ul class="navbar-nav iq-main-menu" id="sidebar-menu">
               <li class="nav-item static-item">
                  <a class="nav-link static-item disabled text-start" href="#" tabindex="-1">
                     <span class="default-icon">MENU</span>
                     <span class="mini-icon" data-bs-toggle="tooltip" title="Home" data-bs-placement="right">-</span>
                  </a>
               </li>

               <li class="nav-item">
                  <a class="nav-link" data-bs-toggle="collapse" href="#producto-menu" role="button" aria-expanded="false" aria-controls="producto-menu">
                     <i class="icon" data-bs-toggle="tooltip" title="Menu Style" data-bs-placement="right">
                        <svg width="20" class="icon-20" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path opacity="0.4" d="M13.6663 6.99992C13.6663 10.6826 10.6817 13.6666 6.99967 13.6666C3.31767 13.6666 0.333008 10.6826 0.333008 6.99992C0.333008 3.31859 3.31767 0.333252 6.99967 0.333252C10.6817 0.333252 13.6663 3.31859 13.6663 6.99992Z" fill="currentColor"/>
                              <path fill-rule="evenodd" clip-rule="evenodd" d="M4.01351 6.20239C3.57284 6.20239 3.21484 6.56039 3.21484 6.99973C3.21484 7.43973 3.57284 7.79839 4.01351 7.79839C4.45418 7.79839 4.81218 7.43973 4.81218 6.99973C4.81218 6.56039 4.45418 6.20239 4.01351 6.20239ZM6.99958 6.20239C6.55891 6.20239 6.20091 6.56039 6.20091 6.99973C6.20091 7.43973 6.55891 7.79839 6.99958 7.79839C7.44024 7.79839 7.79824 7.43973 7.79824 6.99973C7.79824 6.56039 7.44024 6.20239 6.99958 6.20239ZM9.18718 6.99973C9.18718 6.56039 9.54518 6.20239 9.98584 6.20239C10.4265 6.20239 10.7845 6.56039 10.7845 6.99973C10.7845 7.43973 10.4265 7.79839 9.98584 7.79839C9.54518 7.79839 9.18718 7.43973 9.18718 6.99973Z" fill="currentColor"/>
                        </svg>
                     </i>
                     <span class="item-name">Productos</span>
                     <i class="right-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" class="icon-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                        </svg>
                     </i>
                  </a>
                  <ul class="sub-nav collapse" id="producto-menu" data-bs-parent="#sidebar-menu">
                     <li class="nav-item">
                        <a class="nav-link" href="nuevo_producto">
                           <i class="icon"><svg class="icon-10" width="10" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><g><circle cx="12" cy="12" r="8" fill="currentColor"></circle></g></svg></i>
                           <i class="sidenav-mini-icon" data-bs-toggle="tooltip" title="Nuevo producto" data-bs-placement="right"> + </i>
                           <span class="item-name"> Nuevo producto </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="categorias">
                           <i class="icon"><svg class="icon-10" width="10" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><g><circle cx="12" cy="12" r="8" fill="currentColor"></circle></g></svg></i>
                           <i class="sidenav-mini-icon" data-bs-toggle="tooltip" title="Categorías" data-bs-placement="right"> + </i>
                           <span class="item-name"> Categorías </span>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="subcategorias">
                           <i class="icon"><svg class="icon-10" width="10" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><g><circle cx="12" cy="12" r="8" fill="currentColor"></circle></g></svg></i>
                           <i class="sidenav-mini-icon" data-bs-toggle="tooltip" title="Subcategorías" data-bs-placement="right"> + </i>
                           <span class="item-name"> Subcategorías </span>
                        </a>
                     </li>

                     <li class="nav-item">
                        <a class="nav-link" href="ver_productos">
                           <i class="icon"><svg class="icon-10" width="10" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><g><circle cx="12" cy="12" r="8" fill="currentColor"></circle></g></svg></i>
                           <i class="sidenav-mini-icon" data-bs-toggle="tooltip" title="Nueva subcategoría" data-bs-placement="right"> + </i>
                           <span class="item-name"> Ver productos </span>
                        </a>
                     </li>
                  </ul>
               </li>  

               <li class="nav-item">
                     <a class="nav-link" data-bs-toggle="collapse" href="#home-menu" role="button" aria-expanded="false" aria-controls="home-menu">
                        <i class="icon" data-bs-toggle="tooltip" title="Menu Style" data-bs-placement="right">
                           <svg width="20" class="icon-20" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                 <path opacity="0.4" d="M13.6663 6.99992C13.6663 10.6826 10.6817 13.6666 6.99967 13.6666C3.31767 13.6666 0.333008 10.6826 0.333008 6.99992C0.333008 3.31859 3.31767 0.333252 6.99967 0.333252C10.6817 0.333252 13.6663 3.31859 13.6663 6.99992Z" fill="currentColor"/>
                                 <path fill-rule="evenodd" clip-rule="evenodd" d="M4.01351 6.20239C3.57284 6.20239 3.21484 6.56039 3.21484 6.99973C3.21484 7.43973 3.57284 7.79839 4.01351 7.79839C4.45418 7.79839 4.81218 7.43973 4.81218 6.99973C4.81218 6.56039 4.45418 6.20239 4.01351 6.20239ZM6.99958 6.20239C6.55891 6.20239 6.20091 6.56039 6.20091 6.99973C6.20091 7.43973 6.55891 7.79839 6.99958 7.79839C7.44024 7.79839 7.79824 7.43973 7.79824 6.99973C7.79824 6.56039 7.44024 6.20239 6.99958 6.20239ZM9.18718 6.99973C9.18718 6.56039 9.54518 6.20239 9.98584 6.20239C10.4265 6.20239 10.7845 6.56039 10.7845 6.99973C10.7845 7.43973 10.4265 7.79839 9.98584 7.79839C9.54518 7.79839 9.18718 7.43973 9.18718 6.99973Z" fill="currentColor"/>
                           </svg>
                        </i>
                        <span class="item-name">Home</span>
                        <i class="right-icon">
                           <svg xmlns="http://www.w3.org/2000/svg" width="18" class="icon-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                           </svg>
                        </i>
                     </a>
                     <ul class="sub-nav collapse" id="home-menu" data-bs-parent="#sidebar-menu">
                        <li class="nav-item">
                           <a class="nav-link" href="slider_home">
                              <i class="icon"><svg class="icon-10" width="10" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><g><circle cx="12" cy="12" r="8" fill="currentColor"></circle></g></svg></i>
                              <i class="sidenav-mini-icon" data-bs-toggle="tooltip" title="Nuevo producto" data-bs-placement="right"> S </i>
                              <span class="item-name"> Slider Home </span>
                           </a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="banner_home">
                              <i class="icon"><svg class="icon-10" width="10" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><g><circle cx="12" cy="12" r="8" fill="currentColor"></circle></g></svg></i>
                              <i class="sidenav-mini-icon" data-bs-toggle="tooltip" title="Nuevo producto" data-bs-placement="right"> B </i>
                              <span class="item-name"> Banner Home </span>
                           </a>
                        </li>
                        
                        
                     </ul>
               </li>  
               
            </ul>
            <!-- Sidebar Menu End -->        
         </div>
      </div>
      <div class="sidebar-footer"></div>
   </aside>

   <main class="main-content">
      <div class="position-relative  iq-banner ">
      <!--Nav Start-->
            <nav class="nav navbar navbar-expand-xl navbar-light iq-navbar">
               <div class="container-fluid navbar-inner">
                  <a href="home" class="navbar-brand">
                     
                     <!--Logo start-->
                     <div class="logo-main">
                        <div class="logo-normal">
                           <img src="../../images/logo.svg" class="img-fluid" width="150" alt="">
                        </div>
                        <div class="logo-mini">
                           <img src="../../images/logo.svg" class="img-fluid" width="150" alt="">
                        </div>
                     </div>
                     <!--logo End-->
                  </a>
                  <div class="sidebar-toggle" data-toggle="sidebar" data-active="true">
                     <i class="icon d-flex">
                        <svg class="icon-20" width="20" viewBox="0 0 24 24">
                           <path fill="currentColor" d="M4,11V13H16L10.5,18.5L11.92,19.92L19.84,12L11.92,4.08L10.5,5.5L16,11H4Z" />
                        </svg>
                     </i>
                  </div>
                  
                  <div class="d-flex align-items-center">
                     <button id="navbar-toggle" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                        <span class="navbar-toggler-bar bar1 mt-1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </span>
                     </button>
                  </div>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="mb-2 navbar-nav ms-auto align-items-center navbar-list mb-lg-0 ">
                        <li class="nav-item dropdown">
                           <a class="py-0 nav-link d-flex align-items-center ps-3" href="#" id="profile-setting" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              <img src="../assets/images/avatars/01.png" alt="User-Profile" class="img-fluid avatar avatar-50 avatar-rounded" loading="lazy">
                              <div class="caption ms-3 d-none d-md-block ">
                                 <h6 class="mb-0 caption-title">Administrator</h6>
                              </div>
                           </a>
                           <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="profile-setting">
                              <li><a class="dropdown-item" href="#" onclick="cerrar_sesion()">Cerrar sesión</a></li>
                           </ul>
                        </li>
                     </ul>
                  </div>
               </div>
            </nav>
      <!--Nav End-->


            <div class="iq-navbar-header " style="height: 0;"></div>
      </div>