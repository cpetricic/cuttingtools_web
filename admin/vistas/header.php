<!doctype html>
<html lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Administrador - Cutting Tools</title>
<script src="../assets/js/jquery-3.7.1.min.js"></script>
<!-- Favicon -->
<link rel="icon" href="../../favicon.ico" type="image/x-icon" />
<!-- Library / Plugin Css Build -->
<link rel="stylesheet" href="../assets/css/core/libs.min.css">
<!-- qompac-ui Design System Css -->
<link rel="stylesheet" href="../assets/css/qompac-ui.min.css?v=1.0.1">
<!-- Custom Css -->
<link rel="stylesheet" href="../assets/css/custom.min.css?v=1.0.1">
<!-- Dark Css -->
<link rel="stylesheet" href="../assets/css/dark.min.css?v=1.0.1">
<!-- Customizer Css -->
<link rel="stylesheet" href="../assets/css/customizer.min.css?v=1.0.1" >
<!-- RTL Css -->
<link rel="stylesheet" href="../assets/css/rtl.min.css?v=1.0.1">

<!-- <link rel="stylesheet" href="../assets/vendor/richtexteditor/rte_theme_default.css" />
<script type="text/javascript" src="../assets/vendor/richtexteditor/rte.js"></script>
<script type="text/javascript" src='../assets/vendor/richtexteditor/plugins/all_plugins.js'></script> -->

<script src="../assets/js/alert/alertify.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../assets/js/alert/alertify.core.css">
<link rel="stylesheet" type="text/css" href="../assets/js/alert/alertify.default.css">

<script src="../assets/js/global.js" type="text/javascript"></script>
<link rel="stylesheet" href="../css/estilo.css">

<!-- Google Font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">    
</head>
<body class="">
    <!-- loader Start -->
    <div id="loading">
        <div class="loader simple-loader">
            <div class="loader-body ">
                <img src="../assets/images/loader.webp" alt="loader" class="image-loader img-fluid">
            </div>
        </div>
    </div>
