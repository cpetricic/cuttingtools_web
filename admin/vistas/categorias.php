<?php include 'header.php';?>
<?php include 'menu.php';?>

<script>
var url_metodo='../lib/categorias.php';    
$(document).ready(function(){
    get_datos();
});
function get_datos(){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:1},
		dataType: 'json',
		success: function(a){
            var html='<table id="datatable" class="table" data-toggle="data-table">'+
                        '<thead>'+
                            '<tr>'+
                                '<th>ID</th>'+
                                '<th>Nombre</th>'+
                                '<th>Fecha ingreso</th>'+
                                '<th>Estado</th>'+
                                '<th>Acciones</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
			for(var i=0;i<a.length;i++){
                if(a[i].estado==1){
                    var status = '<span class="badge bg-soft-success p-2 text-success pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id+')">Activo</span>';
                }else{
                    var status = '<span class="badge bg-soft-danger p-2 text-danger pointer" onclick="cambio_estado('+a[i].estado+','+a[i].id+')">Inactivo</span>';
                }
                html+='<tr>'+
                        '<td>'+a[i].id+'</td>'+
                        '<td>'+a[i].nombre+'</td>'+
                        '<td>'+a[i].fecha+'</td>'+
                        '<td>'+status+'</td>'+
                        '<td>'+
                            '<button type="button" class="btn btn-sm btn-light" onclick="open_editar('+a[i].id+')">Editar</button>'+
                            '<button type="button" class="btn btn-sm btn-danger" onclick="open_confirm('+a[i].id+')">Eliminar</button>'+
                        '</td>'+
                     '</tr>';
            }
            html+='</tbody></table>';
            $('#body_table').html(html);

            $('[data-toggle="data-table"]').DataTable({
                "language": {
                    "url": '../assets/vendor/language/es-ES.json'
                }
            });
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}    
function ingresar(){
    var nombre = $('#nombre_add').val();
    if(nombre==''){
        error('Debe ingresar el nombre');
        $('#nombre_add').focus();
        return;
    }
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:6, nombre:nombre},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Ingresado correctamente');
					$('#nombre_add').val('');
                    get_datos();
                    $('#nuevoModal').modal('hide');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function cambio_estado(estado, id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:4, id:id, estado:estado},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Cambiado correctamente');
                    get_datos();
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function eliminar(id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:3, id:id},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Eliminado correctamente');
                    get_datos();
                    $('#confirmModal').modal('hide');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function open_confirm(id){
    $('#btn_confirm_si').attr('onclick', 'eliminar('+id+')');
    $('#confirmModal').modal('show');
}
function open_editar(id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:2, id:id},
		dataType: 'json',
		success: function(a){
            $('#nombre_edit').val(a.nombre);

            $('#btn_editar').attr('onclick', 'editar('+id+')');
            $('#editarModal').modal('show');
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function editar(id){
    var nombre = $('#nombre_edit').val();
    if(nombre==''){
        error('Debe ingresar el nombre');
        $('#nombre_edit').focus();
        return;
    }
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:5, nombre:nombre, id:id},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Editado correctamente');
					$('#nombre_edit').val('');
                    get_datos();
                    $('#editarModal').modal('hide');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
</script>

<div class="content-inner container-fluid pb-0">

    <div class="card">
         <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Categorías</h4>
            </div>
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#nuevoModal">Nueva categoría</button>
         </div>
         <div class="card-body">
            <div id="body_table" class="table-responsive border rounded"></div>
        </div>
    </div>
</div> 


<div class="modal fade" id="nuevoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nueva categoría</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label class="form-label" for="nombre_add">Nombre</label>
                    <input type="text" class="form-control" id="nombre_add" placeholder="">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-success" onclick="ingresar()">Guardar</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Editar categoría</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <input type="hidden" class="form-control" id="id_edit" placeholder="">
            <form>
                <div class="form-group">
                    <label class="form-label" for="nombre_edit">Nombre</label>
                    <input type="text" class="form-control" id="nombre_edit" placeholder="">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-success" id="btn_editar">Guardar</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="confirmModalLabel">Eliminar</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button id="btn_confirm_si" type="button" class="btn btn-success">Si</button>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php';?>   

