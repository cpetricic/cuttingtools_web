<?php include 'header.php';?>
<?php include 'menu.php';?>
<script>
var url_metodo='../lib/producto.php';     
var editor
$(document).ready(function(){
    editor = new RichTextEditor("#descripcion");
    $('.select_categoria').change(function(){
        var id_categoria = $('.select_categoria').val();
        if(id_categoria==''){
            $('.select_subcategoria').html('');
        }else{
            get_subcategorias_activas(id_categoria);
        }
    });
    get_info();
});
function get_info(){
    var id = $('#id_hidden').val();
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:8, id:id},
		dataType: 'json',
		success: function(a){
            $('#nombre').val(a.nombre);
            $('#marca').val(a.marca);
            $('#grupo').val(a.id_grupo);
            $('#sku').val(a.sku);
            $('#precio_venta').val(a.precio_venta);
            $('#precio_normal').val(a.precio_normal);
            //console.log(a.descripcion);
            editor.setHTMLCode(a.descripcion);
            get_categorias_activas(a.id_categoria, a.id_subcategoria);

            var html='';
            for(var i=0;i<a.images.length;i++){
                html+='<div id="img_'+a.images[i].id+'" class="col-md-4 col-lg-2">'+
                            '<div class="card mb-3">'+
                                '<div class="img-preview" style="background-image:url(../../images/producto/'+a.images[i].imagen+')"></div>'+
                                '<div class="card-body">'+
                                    '<button class="btn btn-danger" onclick="open_confirm('+a.images[i].id+')">Eliminar</button>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            }
            $('#container_img_prod').html(html);
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function get_categorias_activas(id_categoria, id_subcategoria){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:1},
		dataType: 'json',
		success: function(a){
            var html='<option value="">Seleccione una categoría</option>';
            var sel_subcategoria = false;
			for(var i=0;i<a.length;i++){
                if(id_categoria==a[i].id){
                    html+='<option value="'+a[i].id+'" selected>'+a[i].nombre+'</option>';
                    sel_subcategoria=true;
                }else{
                    html+='<option value="'+a[i].id+'">'+a[i].nombre+'</option>';
                } 
            }
            $('.select_categoria').html(html);
            if(sel_subcategoria){
                get_subcategorias_activas(id_categoria, id_subcategoria);
            }
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
} 
function get_subcategorias_activas(id_categoria, id_subcategoria){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:2, id_categoria:id_categoria},
		dataType: 'json',
		success: function(a){
            var html='<option value="">Seleccione una subcategoría</option>';
			for(var i=0;i<a.length;i++){
                if(id_subcategoria==a[i].id){
                    html+='<option value="'+a[i].id+'" selected>'+a[i].nombre+'</option>';
                }else{
                    html+='<option value="'+a[i].id+'">'+a[i].nombre+'</option>';
                }
            }
            $('.select_subcategoria').html(html);
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
function open_confirm(id){
    $('#btn_confirm_si').attr('onclick', 'eliminar_img('+id+')');
    $('#confirmModal').modal('show');
} 
function eliminar_img(id){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:9, id:id},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
					ok('Eliminado correctamente');
                    $('#img_'+id).remove();
                    $('#confirmModal').modal('hide');
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}


function ingresar(){
    var id_producto = $('#id_hidden').val();
    var grupo = $('#grupo').val();
    var nombre = $('#nombre').val();
    var marca = $('#marca').val();
    var sku = $('#sku').val();
    var precio_venta = $('#precio_venta').val();
    var precio_normal = $('#precio_normal').val();
    var posicion = $('#posicion').val();
    var id_categoria = $('#categoria').val();
    var id_subcategoria = $('#subcategoria').val();
    var descripcion = editor.getHTMLCode();

    if(nombre==''){
        error('Debe ingresar el nombre');
        $('#nombre').focus();
        return;
    }
    if(marca==''){
        error('Debe ingresar la marca');
        $('#marca').focus();
        return;
    }
    if(sku==''){
        error('Debe ingresar el SKU');
        $('#sku').focus();
        return;
    }
    if(precio_venta==''){
        error('Debe ingresar el precio de venta');
        $('#precio_venta').focus();
        return;
    }
    if(precio_normal==''){
        error('Debe ingresar el precio normal');
        $('#precio_normal').focus();
        return;
    }
    if(posicion==''){
        error('Debe ingresar la posición');
        $('#posicion').focus();
        return;
    }

    if(id_categoria==''){
        error('Debe seleccionar una categoria');
        $('#categoria').focus();
        return;
    }
    if(grupo==''){
        grupo=0;
    }
    
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:10, grupo:grupo, id_producto:id_producto, nombre:nombre, marca:marca, sku:sku, precio_venta:precio_venta, precio_normal:precio_normal, posicion:posicion, id_categoria:id_categoria, id_subcategoria:id_subcategoria, descripcion:descripcion},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				case 1:
                    if($('#images').prop('files').length>0){
                        upload_img(id_producto);
                    }else{
                        ok('Editado correctamente');
                        top.location.href="ver_productos";
                    }
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
} 
function upload_img(id_producto){
    var images = $('#images').prop('files');
    var form_data = new FormData();
    for(var i=0;i<images.length;i++){
        form_data.append(i, $('#images').prop('files')[i]);
    }

    $.ajax({
        url: '../lib/upload_img.php',
        type: 'POST',
        data: form_data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            data = JSON.parse(data);
            //console.log(data);
            var imagenes_ok=[];
            for(i=0;i<data.length;i++){
                if(data[i].estado==1){
                    imagenes_ok.push(data[i].nombre_modificado);
                }
            }
            insert_image(id_producto, imagenes_ok);
        },
    });
}
function insert_image(id_producto, imagenes_ok){
    $.ajax({
		type: 'POST',
		url: url_metodo,
		data: {idfuncion:4, id_producto:id_producto, imagenes:imagenes_ok},
		dataType: 'json',
		success: function(a){
			switch(a.estado){
				case 0:
					error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
				break;
				default:
					ok('Editado correctamente');
                    top.location.href="ver_productos";
				break;
			}
        },
        error: function(){
            error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
        }
    });
}
</script>
<input type="hidden" id="id_hidden" value="<?php echo $_GET['i'];?>">
<div class="content-inner container-fluid pb-0">
    <div class="card">
         <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Editar producto</h4>
            </div>
            <button type="button" class="btn btn-success" onclick="ingresar()">Guardar</button>
         </div>
         <div class="card-body">
            <form>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Nombre Producto</label>
                            <input type="text" class="form-control" id="nombre" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Marca</label>
                            <input type="text" class="form-control" id="marca" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">SKU</label>
                            <input type="text" class="form-control" id="sku" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Precio de venta</label>
                            <input type="number" class="form-control" id="precio_venta" value="0" min="0" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Precio normal</label>
                            <input type="number" class="form-control" id="precio_normal" value="0" min="0" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Posición</label>
                            <input type="number" class="form-control" id="posicion" value="1" min="1" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Categoría</label>
                            <select id="categoria" class="form-select mb-3 shadow-none select_categoria"></select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Subategoría</label>
                            <select id="subcategoria" class="form-select mb-3 shadow-none select_subcategoria"></select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-label">Grupo (valor 0 sin grupo)</label>
                            <input type="number" class="form-control" id="grupo" value="0" min="0" placeholder="">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-label">Especificaciones</label>
                            <div id="descripcion" style="max-width:100%"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title mt-2">Imagenes producto</h4>
            </div>
        </div>
        <div class="card-body">
            <form enctype="multipart/form-data">
                <input class="form-control" type="file" name="file" id="images" accept="image/*" multiple>
            </form>
            <div id="container_img_prod" class="row mt-5"></div>
        </div>
    </div>
</div> 


<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="confirmModalLabel">Eliminar</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
        <button id="btn_confirm_si" type="button" class="btn btn-success">Si</button>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php';?>   

