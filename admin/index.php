<!doctype html>
<html lang="en" dir="ltr">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Administrador - Cutting Tools</title>

<script src="assets/js/jquery-3.7.1.min.js"></script>
<!-- Favicon -->
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<!-- Library / Plugin Css Build -->
<link rel="stylesheet" href="assets/css/core/libs.min.css">
<!-- qompac-ui Design System Css -->
<link rel="stylesheet" href="assets/css/qompac-ui.min.css?v=1.0.1">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/custom.min.css?v=1.0.1">
<!-- Dark Css -->
<link rel="stylesheet" href="assets/css/dark.min.css?v=1.0.1">
<!-- Customizer Css -->
<link rel="stylesheet" href="assets/css/customizer.min.css?v=1.0.1" >
<!-- RTL Css -->
<link rel="stylesheet" href="assets/css/rtl.min.css?v=1.0.1">

<script src="assets/js/alert/alertify.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="assets/js/alert/alertify.core.css">
<link rel="stylesheet" type="text/css" href="assets/js/alert/alertify.default.css">

<!-- Google Font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">  
<script>
function login(){
   var user = $('#email_login').val();
   var pass = $('#password_login').val();

   if(user==''){
      error('Debe ingresar el email');
      $('#email_login').focus();
      return;
   }
   if(pass==''){
      error('Debe ingresar la contraseña');
      $('#password_login').focus();
      return;
   }
   btn_enviando('btn_iniciar_sesion');
   $.ajax({
      type: 'POST',
      url: 'lib/login.php',
      data: {idfuncion:1, user:user, pass:pass},
      dataType: 'json',
      success: function(a){
         switch(a.estado){
            case 0:
               error('<strong>Error</strong>, la información es inválida.');
            break;
            case 1:
               sessionStorage.setItem("id_usuario_admin", a.id_usuario);
               sessionStorage.setItem("email_usuario_admin", a.email_usuario);
               sessionStorage.setItem("nombre_usuario_admin", a.nombre_usuario);

               top.location.href="vistas/home";
            break;
            case 2:
               error('Completa el proceso de activación de tu cuenta.');
            break;
         }
         btn_reset('btn_iniciar_sesion');
      },
      error: function(){
         error('<strong>Error</strong>, vuelva a intentarlo más tarde.');
         btn_reset('btn_iniciar_sesion');
      }
   });
} 
function error(text){
    alertify.error(text);
}
function ok(text){
    alertify.success(text);
}
function btn_enviando(btn){
    $('#'+btn).prop('disabled', true);
    txt_init = $('#'+btn).text();
    var txt_loading = $('#'+btn).attr('data-loading-text');
    $('#'+btn).html(txt_loading);
}
function btn_reset(btn){
    $('#'+btn).prop('disabled', false);
    $('#'+btn).html(txt_init);
} 
</script>  
</head>
<body class="">
    <!-- loader Start -->
    <div id="loading">
    <div class="loader simple-loader">
        <div class="loader-body ">
                <img src="assets/images/loader.webp" alt="loader" class="image-loader img-fluid">
        </div>
    </div>
    </div>

    <div class="wrapper">
      <section class="login-content overflow-hidden">
         <div class="row no-gutters align-items-center justify-content-center" style="height:100vh;background-color:#EAEAEA">            
            <div class="col-md-12 col-lg-6 align-self-center" style="max-width: 800px;">
               <a href="login" class="navbar-brand d-flex align-items-center mb-3 justify-content-center text-primary">
                  <div class="logo-normal">
                     <img src="../images/logo.svg" class="img-fluid" alt="">
                  </div>
               </a>
               <div class="row justify-content-center pt-5">
                  <div class="col-md-12">
                     <div class="card  d-flex justify-content-center mb-0 auth-card iq-auth-form bg-white">
                        <div class="card-body ">                          
                           <h2 class="mb-2 text-center">Administrador</h2>
                           <p class="text-center">Inicia sesion en el administrador.</p>
                           <form>
                              <div class="row">
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                       <label for="email" class="form-label">Email</label>
                                       <input type="email" class="form-control" id="email_login" aria-describedby="email" placeholder="xyz@example.com">
                                    </div>
                                 </div>
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                       <label for="password" class="form-label">Password</label>
                                       <input type="password" class="form-control" id="password_login" aria-describedby="password" placeholder="xxxx">
                                    </div>
                                 </div>
                              </div>
                              <div class="d-flex justify-content-center">
                                 <button id="btn_iniciar_sesion" type="button" class="btn btn-warning" data-loading-text="Ingresando..." onclick="login()">Iniciar</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
    </div>
    <!-- Library Bundle Script -->
    <script src="assets/js/core/libs.min.js"></script>
    <!-- Plugin Scripts -->
    
    <!-- Slider-tab Script -->
    <script src="assets/js/plugins/slider-tabs.js"></script>
    
    <!-- Lodash Utility -->
    <script src="assets/vendor/lodash/lodash.min.js"></script>
    <!-- Utilities Functions -->
    <script src="assets/js/iqonic-script/utility.min.js"></script>
    <!-- Settings Script -->
    <script src="assets/js/iqonic-script/setting.min.js"></script>
    <!-- Settings Init Script -->
    <script src="assets/js/setting-init.js"></script>
    <!-- External Library Bundle Script -->
    <script src="assets/js/core/external.min.js"></script>
    <!-- Widgetchart Script -->
    <script src="assets/js/charts/widgetcharts.js?v=1.0.1" defer></script>
    <!-- Dashboard Script -->
    <script src="assets/js/charts/dashboard.js?v=1.0.1" defer></script>
    <!-- qompacui Script -->
    <script src="assets/js/qompac-ui.js?v=1.0.1" defer></script>
    <script src="assets/js/sidebar.js?v=1.0.1" defer></script>
    
  </body>
</html>
