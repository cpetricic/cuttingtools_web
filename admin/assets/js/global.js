$(document).ready(function(){
    verificar_sesion();
});
function error(text){
    alertify.error(text);
}
function ok(text){
    alertify.success(text);
}
function btn_enviando(btn){
    $('#'+btn).prop('disabled', true);
    txt_init = $('#'+btn).text();
    var txt_loading = $('#'+btn).attr('data-loading-text');
    $('#'+btn).html(txt_loading);
}
function btn_reset(btn){
    $('#'+btn).prop('disabled', false);
    $('#'+btn).html(txt_init);
}
function cerrar_sesion(){
    sessionStorage.removeItem("id_usuario_admin");
    sessionStorage.removeItem("email_usuario_admin");
    sessionStorage.removeItem("nombre_usuario_admin");

    top.location.href="../login";
}
function verificar_sesion(){
    if(sessionStorage.getItem("id_usuario_admin")===null){
        top.location.href="../login";
    }
}