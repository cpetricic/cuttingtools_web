<?php
//$cap = explode('/',$_SERVER['REQUEST_URI']);

// echo $cap[0].'-<br>';
// echo $cap[1].'-<br>';
// echo $cap[2].'-<br>';
// echo $cap[3].'-<br>';

// $_GET['cat'] = $cap[2];
// $_GET['sub'] = $cap[3];
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Cutting Tools &#8211; Especialistas en Herramientas de Cortes</title>
<?php include 'scripts.php';?>
<script>
$(document).ready(function(){
    $('#btn_filter').click(function(){
        $('.container_filter').slideToggle(200);
    });
    set_categorias();
    if(sessionStorage.getItem("pag")===null){
        sessionStorage.setItem("pag", 1);
    }
    get_productos();
});  
function set_categorias(){
    var categoria = $('#categoria_hidden').val();
	var subcategoria = $('#subcategoria_hidden').val();
	$.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:10},
		dataType: 'json',
		success: function(a){
            //?cat=$1&sub=$2&pag=$3
			var html='';
            var bread = '<li class="breadcrumb-item"><a class="color-FFBC07" href="home">Home</a></li>';
			for(var i=0;i<a.length;i++){
				if(a[i].subcategorias.length>0){
                    if(categoria==a[i].id_categoria){
                        var htmlopen="show";
                        var collapsed="";
                        bread+='<li class="breadcrumb-item"><a class="color-FFBC07" href="productos?cat='+a[i].id_categoria+'">'+a[i].nombre_categoria+'</a></li>';
                    }else{
                        var htmlopen="";
                        var collapsed="collapsed";
                    }

					var html_sub='';
					for(var j=0;j<a[i].subcategorias.length;j++){
                        if(subcategoria==a[i].subcategorias[j].id_subcategoria){
                            bread+='<li class="breadcrumb-item"><a class="color-FFBC07" href="productos?cat='+a[i].id_categoria+'&sub='+a[i].subcategorias[j].id_subcategoria+'">'+a[i].subcategorias[j].nombre_subcategoria+'</a></li>';
                        }
						html_sub+='<li class="list-group-item"><a href="productos?cat='+a[i].id_categoria+'&sub='+a[i].subcategorias[j].id_subcategoria+'" class="color-000">'+a[i].subcategorias[j].nombre_subcategoria+'</a></li>';
					}
                    
					html+='<div class="accordion-item">'+
								'<h2 class="accordion-header">'+
									'<button class="accordion-button '+collapsed+'" type="button" data-bs-toggle="collapse" data-bs-target="#prodcat_'+a[i].id_categoria+'" aria-expanded="false" aria-controls="prodcat_'+a[i].id_categoria+'">'+a[i].nombre_categoria+'</button>'+
								'</h2>'+
								'<div id="prodcat_'+a[i].id_categoria+'" class="accordion-collapse collapse '+htmlopen+'" data-bs-parent="#acordion_categorias">'+
									'<div class="accordion-body">'+
										'<ul class="list-group list-group-flush">'+html_sub+'</ul>'+
									'</div>'+
								'</div>'+
							'</div>';
				}else{
					html+='<div class="accordion-item">'+
								'<h2 class="accordion-header">'+
								'<a href="productos?cat='+a[i].id_categoria+'" class="link_menu">'+a[i].nombre_categoria+'</a>'+
								'</h2>'+
							'</div>';
				}
			}
			$('#acordion_categorias').html(html);
			$('#container_breadcrumb').html(bread);
        },
        error: function(){
            
        }
    });
}  
function get_productos(){
    var categoria = $('#categoria_hidden').val();
	var subcategoria = $('#subcategoria_hidden').val();
	var orden = $('#orden_prod').val();
	var busqueda = $('#busqueda_hidden').val();
	var pag = $('#pag_hidden').val();
    if(pag==''){
        pag=1;
    }
    $.ajax({
		type: 'POST',
		url: 'lib/modulo.php',
		data: {idfuncion:11, orden:orden, categoria:categoria, subcategoria:subcategoria, pag:pag, busqueda:busqueda},
		dataType: 'json',
		success: function(a){
            create_paginacion(a.total);
            var html='';
			if(a.datos.length>0){
                for(var i=0;i<a.datos.length;i++){
                    html+='<div class="col-12 col-lg-6 col-xl-3 mb20">'+
                            '<a href="detalle_producto/'+a.datos[i].id_producto+'" class="box-producto">'+
                                '<div class="img-producto" style="background-image: url(images/producto/'+a.datos[i].imagen+')"></div>'+
                                '<div class="box-info">'+
                                    '<p class="marca">'+a.datos[i].marca+'</p>'+
                                    '<p class="nombre_producto">'+a.datos[i].nombre+'</p>'+
                                    '<div class="line"></div>'+
                                    '<p class="sku">SKU: <b>'+a.datos[i].sku+'</b></p>'+

                                    '<p class="precio_venta">$'+a.datos[i].precio_venta+'</p>'+
                                    '<p class="precio_normal">NORMAL: <b>$'+a.datos[i].precio_normal+'</b></p>'+

                                    '<button type="button" class="btn btn-warning btn-mod" onclic="add_cart('+a.datos[i].id_producto+')">Ver producto</button>'+
                                '</div>'+
                            '</a>'+
                        '</div>';
                }
                
            }else{
                html='<div class="col-12 mt50 mb20 text-center"><h4>No se encontraron productos</h4></div>';
            }
            $('#container_prod').html(html);
        },
        error: function(){
        }
    });
}
function create_paginacion(total){
    var categoria = $('#categoria_hidden').val();
	var subcategoria = $('#subcategoria_hidden').val();
    //?cat=$1&sub=$2&pag=$3
    var url='productos?cat='+categoria+'&sub='+subcategoria+'&pag=';
    var pag = $('#pag_hidden').val();
    if(pag==''){
        pag=1;
    }
    pag = parseInt(pag);
    var xpag=8;
    var cant_pag = Math.ceil(total/xpag);

    

    if(total<=xpag){
        $('.contenedor_pag').html('');
    }else{
        var html='<nav aria-label="Page navigation">'+
                    '<ul class="pagination">';

        if(pag>1){
            html+='<li class="page-item"><a class="page-link" href="'+url+(pag-1)+'" aria-label="Previous"><span aria-hidden="true"><i class="bi bi-arrow-left"></i></span></a></li>';
        }            
        for(var i=0;i<cant_pag;i++){
            console.log(pag);
            if(pag==(i+1)){
                html+='<li class="page-item active"><a class="page-link" href="'+url+(i+1)+'">'+(i+1)+'</a></li>';
            }else{
                html+='<li class="page-item"><a class="page-link" href="'+url+(i+1)+'">'+(i+1)+'</a></li>';
            }
        }            
        if(cant_pag!=pag){
            html+='<li class="page-item"><a class="page-link" href="'+url+(pag+1)+'" aria-label="Next"><span aria-hidden="true"><i class="bi bi-arrow-right"></i></span></a></li>';
        }
                            
        html+='</ul></nav>';

        $('.contenedor_pag').html(html);
    }
}
</script>
</head>
<body>
<?php include 'header.php';?>
<input type="hidden" id="url_hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
<input type="hidden" id="categoria_hidden" value="<?php echo $_GET['cat']; ?>">
<input type="hidden" id="subcategoria_hidden" value="<?php echo $_GET['sub']; ?>">
<input type="hidden" id="pag_hidden" value="<?php echo $_GET['pag']; ?>">
<input type="hidden" id="busqueda_hidden" value="<?php echo $_GET['s']; ?>">
<div class="container-fluid cont_main bg-F7F7F7">
    <div class="container-fliud pt40">
        <div class="row ml0 mr0 ">
            <div class="col-lg-3 bg-FFF pt10 pb10 pl30 pr30 mb20 block-m">
                <button id="btn_filter" type="button" class="btn btn-light ml-auto d-block"><i class="bi bi-filter"></i></button>
            </div>
            <div class="col-lg-3 bg-FFF pt20 pb20 pl30 pr30 mb20 container_filter">
                <nav aria-label="breadcrumb" class="size12">
                    <ol id="container_breadcrumb" class="breadcrumb"></ol>
                </nav>
                
                <p class="titulo_filtro">Ordenar por:</p>
                <select id="orden_prod" class="form-select mb30" onchange="get_productos()">
                    <option value="0">Recomendado</option>
                    <option value="1">Menor precio</option>
                    <option value="2">Mayor precio</option>
                </select>

                <p class="titulo_filtro">Categorías</p>
                <div class="accordion" id="acordion_categorias"></div>
            </div>
            <div class="col-lg-9">
                <div id="container_prod" class="row ml0 mr0"></div>
            </div>
        </div>

        <div class="row ml0 mr0 mt40 justify-content-center">
            <div class="col-auto contenedor_pag"></div>
        </div>

    </div>
</div>    
<?php include 'footer.php';?>
</body>
</html>